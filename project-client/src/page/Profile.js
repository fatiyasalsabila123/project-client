import React, { useEffect } from 'react'
import Sidebar from '../component/Sidebar'
import Navbar from '../component/Navbar'
import { useState } from 'react'
import { useHistory } from "react-router-dom";
import axios from 'axios';
import Swal from 'sweetalert2';
import { Modal } from 'react-bootstrap';
import bcrypt from "bcryptjs";

export default function Profile() {
    const[username, setUserName] = useState("");
    const[show, setShow] = useState(false);
    const[show1, setShow1] = useState(false);
    const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
    const[password, setPassword] = useState("");
    const[alamat, setAlamat] = useState("");
    const[tanggalLahir, setTanggalLahir] = useState("");
    const history = useHistory();
    const[email, setEmail] = useState("");
    const[foto, setFoto] = useState(null);
    const [passwordType, setPasswordType] = useState("password");
    const [passwordType1, setPasswordType1] = useState("password");
    const [passwordType2, setPasswordType2] = useState("password");
    
  const [passLama, setPassLama] = useState("");
  const [conPassLama, setConPassLama] = useState("");
  const [passBaru, setPassBaru] = useState("");
  const [conPassBaru, setConPassBaru] = useState("");

    const[profile, setProfile] = useState({
        username:"",
        email:"",
        alamat:"",
        tanggalLahir:"",
        password:"",
        foto: null,
    })

    const getAll = async () => {
        await axios
          .get("http://localhost:1212/register/" + localStorage.getItem("userId"))
          .then((res) => {
            setProfile(res.data.data);
          })
          .catch((error) => {
            alert("Terjadi Kesalahan" + error);
          });
      };
    
      useEffect(() => {
        getAll();
      }, []);

    useEffect(() => {
        axios
          .get("http://localhost:1212/register/" + localStorage.getItem("userId"))
          .then((response) => {
            const profil = response.data.data;
            setUserName(profil.username);
            setEmail(profil.email);
            setAlamat(profil.alamat);
            setTanggalLahir(profil.tanggalLahir);
            setFoto(profile.foto);
          })
          .catch((error) => {
            alert("Terjadi Kesalahan " + error);
          });
      }, []);

      const putProfile = async (e) => {
        e.preventDefault();
        e.persist();
    
        try {
          await axios.put(
            `http://localhost:1212/register/` + localStorage.getItem("userId"),
            {
              username:username,
              email: email,
              alamat: alamat,
              tanggalLahir: tanggalLahir,
            }
          );
          setShow(false);
          Swal.fire({
            icon: "success",
            title: "Berhasil Mengedit",
            showConfirmButton: false,
            timer: 1500,
          });
          setTimeout(() => {
            history.push("/profile");
            window.location.reload();
          }, 1500);
        } catch (error) {
          console.log(error);
        }
      };

      const put = async (e) => {
        e.preventDefault();
        e.persist();
    
        const data = new FormData();
        data.append("file", foto);
    
        console.log(foto);
        try {
          await axios.put(
            `http://localhost:1212/register/foto/${localStorage.getItem("userId")}`,
            data
          );
          setShow(false);
          Swal.fire({
            icon: "success",
            title: "berhasil mengedit",
            showConfirmButton: false,
            timer: 1500,
          });
          setTimeout(() => {
            window.location.reload();
          }, 1500);
        } catch (err) {
          console.log(err);
        }
      };

      const conPassword = async () => {
        await axios
          .get("http://localhost:1212/register/" + localStorage.getItem("userId"))
          .then((res) => {
            setPassLama(res.data.data.password);
          });
      };
      useEffect(() => {
        conPassword();
      }, []);

      const ubahPass = (e) => {
        e.preventDefault();
        bcrypt.compare(conPassLama, passLama, function (err, isMatch) {
          if (err) {
            throw err;
          } else if (!isMatch) {
            Swal.fire({
              icon: "error",
              title: "Password Tidak sama dengan yang sebelumnya",
              showConfirmButton: false,
              timer: 1500,
            });
          } else {
            if (passBaru === conPassLama) {
              Swal.fire({
                icon: "error",
                title: "Password tidak boleh sama dengan sebelumnya",
                showConfirmButton: false,
                timer: 1500,
              });
            } else {
              if (passBaru === conPassBaru) {
                axios
                  .put(
                    "http://localhost:1212/register/password/" +
                      localStorage.getItem("userId"),
                    {
                      password: passBaru,
                    }
                  )
                  .then(() => {
                    Swal.fire({
                      icon: "success",
                      title: " Berhasil!!!",
                      showConfirmButton: false,
                      timer: 1500,
                    });
                    setTimeout(() => {
                      window.location.reload();
                    }, 1500);
                  })
                  .catch((err) => {
                    Swal.fire({
                      icon: "error",
                      title:
                        "Password minimal 8-20 karater, angka, huruf kecil",
                      showConfirmButton: false,
                      timer: 1500,
                    });
                    console.log(err);
                  });
              } else {
                Swal.fire({
                  icon: "error",
                  title: "Password Tidak sama",
                  showConfirmButton: false,
                  timer: 1500,
                });
              }
            }
          }
        });
      };

      const togglePassword = () => {
        console.log(passwordType);
        if (passwordType === "password") {
          setPasswordType("text");
          return;
        }
        setPasswordType("password");
      };

      const togglePassword1 = () => {
        if (passwordType1 === "password") {
          setPasswordType1("text");
          return;
        }
        setPasswordType1("password");
      };
    
      const togglePassword2 = () => {
        if (passwordType2 === "password") {
          setPasswordType2("text");
          return;
        }
        setPasswordType2("password");
      };
    
  return (
    <div className='flex'>
        <div>
            <Sidebar/>
        </div>
        <div className='overflow-x-auto h-[39rem] w-full'>
        <Navbar/>
            <p className='text-center font-bold text-blue-400 text-5xl ml-2 mt-10'>Profile</p>
            <div className='flex gap-[6rem] mb-10'>
            <div className='mt-[5rem]'>
                <p className='ml-5 text-xl font-bold'>Detail</p>
                <p className='ml-5 mt-4 font-bold'>Name : </p>
                <p className='ml-5 mt-2 text-medium'>{profile.username} </p>
                <p className='ml-5 mt-4 font-bold'>Email : </p>
                <p className='ml-5 mt-2 text-medium'>{profile.email}</p>
                <p className='ml-5 mt-4 font-bold'>Tanggal Lahir : </p>
                {profile.tanggalLahir ? (
                <p className='ml-5 mt-2 text-medium'>{profile.tanggalLahir}</p>
                ):(
                  <p className='ml-5 mt-2 text-medium'>Masukan Tanggal Lahir</p>
                )}
                <p className='ml-5 mt-4 font-bold'>Alamat : </p>
                {profile.alamat ? (
                <p className='ml-5 mt-2 text-medium mb-5'>{profile.alamat} </p>
                ):(
                  <p className='ml-5 mt-2 text-medium mb-5 '>Masukan Alamat </p>
                )}
                 <a href={'/editProfile/' + profile.id} class="rounded-r-full ... mt-[2rem] hover:bg-white border-blue-400 p-2 font-bold border border-2 bg-blue-400 hover:text-black text-white">Edit Profile</a>
            </div>
            <div className='mt-[5rem]'>
                <p className=' text-xl font-bold'>About</p>
                <p className='mt-2 text-medium w-[20rem]'>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                <div className='mt-3 text-sm'>
                <button class="rounded-full ... hover:bg-white border-blue-400 p-2 font-bold border border-2 bg-blue-400 hover:text-black text-white" onClick={() => setShow1(true)}>Ganti Password</button>
                </div>
            </div>
            <div className=''>
              <button onClick={handleShow}>
              {profile.foto ? (
                <img src={profile.foto} alt="" className='w-[1rem] border-blue-300 border border-4 max-w-[12rem] min-w-[12rem] mt-2 z-10 ml-[4rem] rounded-full'/>
              ):(
                <img src="https://cdna.artstation.com/p/assets/images/images/034/457/364/large/shin-min-jeong-.jpg?1612345089" alt="" className='w-[1rem] border-blue-300 border border-4 max-w-[12rem] min-w-[12rem] mt-2 z-10 ml-[4rem] rounded-full' />
              )}
              </button>
                <div className='bg-blue-400 text-white -mt-[6rem] z-0 p-3 w-[20rem] h-[25rem]'>
                    <div className='mt-[6rem]'>
                    <p className='text-2xl text-center font-bold'>Hallo Saya</p>
                    <p className='text-2xl text-center font-bold'>{profile.username}</p>
                    </div>
                    <p className='text-center mt-3'>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia</p>
                </div>
              
            </div>
            
            <>
                {/* edit profile */}
                <Modal
                show={show}
                onHide={handleClose}
                id="authentication-modal"
                tabIndex="-1"
                aria-hidden="true"
                className="md:ml-[30%] new ml-0 fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full"
              >
                <div className="relative w-full h-full max-w-md md:h-auto">
                  <div className="relative bg-white rounded-lg shadow dark:bg-white">
                    <button
                      type="button"
                      className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white"
                      data-modal-hide="authentication-modal"
                      onClick={handleClose}
                    >
                      <svg
                        aria-hidden="true"
                        className="w-5 h-5"
                        fill="currentColor"
                        viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          fillRule="evenodd"
                          d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                          clipRule="evenodd"
                        ></path>
                      </svg>
                      <span className="sr-only">Close modal</span>
                    </button>
                    <div className="px-6 py-6 lg:px-8">
                      <h3 className="mb-4 text-xl font-medium text-black dark:text-black">
                        Edit Photo Profile
                      </h3>
                      <form className="space-y-3" onSubmit={put}>
                        <div>
                          <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                           Photo Profile
                          </label>
                          <input
                            onChange={(e) => setFoto(e.target.files[0])}
                            type='file'
                            className="bg-white border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                            required
                          />
                        </div>
                        <button
                          type="submit"
                          className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                        >
                          Tambah
                        </button>
                      </form>
                    </div>
                  </div>
                </div>
              </Modal>  

                          {/* Modal Password */}
            <Modal
              show={show1}
              onHide={!show1}
              id="authentication-modal"
              tabIndex="-1"
              aria-hidden="true"
              className="md:ml-[30%] ml-0 fixed top-0 left-0 right-0 z-50 hidden p-4 md:inset-0 h-modal md:h-full focus:outline-none focus:ring-0"
            >
              <div className="relative w-full h-full max-w-md md:h-auto focus:outline-none focus:ring-0">
                <div className="relative bg-white rounded-lg shadow">
                  <button
                    type="button"
                    className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:text-white"
                    data-modal-hide="authentication-modal"
                    onClick={() => setShow1(false)}
                  >
                    <svg
                      aria-hidden="true"
                      className="w-5 h-5"
                      fill="currentColor"
                      viewBox="0 0 20 20"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        fillRule="evenodd"
                        d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                        clipRule="evenodd"
                      ></path>
                    </svg>
                    <span className="sr-only">Close modal</span>
                  </button>
                  <div className="px-6 py-6 lg:px-8">
                    <h3 className="mb-4 text-xl font-medium text-gray-900 dark:text-black">
                      Edit
                    </h3>
                    <form onSubmit={ubahPass} className="space-y-3">
                      <div>
                        <label htmlFor="password">Password Lama</label>
                        <div className="relative border-none focus:outline-none focus:ring-0">
                          <input
                            required
                            placeholder="Masukan password lama anda"
                            type={passwordType2}
                            className="md:h-5 w-full rounded-lg border p-4 pr-12 text-sm shadow-sm focus:outline-none focus:ring-0"
                            onChange={(e) => setConPassLama(e.target.value)}
                          />

                          <span
                            onClick={togglePassword2}
                            className="absolute inset-y-0 right-0 grid place-content-center px-4 focus:outline-none focus:ring-0"
                          >
                            {passwordType2 === "password" ? (
                              <>
                                <i className="fa-solid fa-eye-slash"></i>
                              </>
                            ) : (
                              <>
                                <i className="fa-solid fa-eye"></i>
                              </>
                            )}
                          </span>
                        </div>
                      </div>
                      <div>
                        <label htmlFor="password">Password Baru</label>
                        <div className="relative">
                          <input
                            required
                            placeholder="Masukan password baru"
                            type={passwordType}
                            className="md:h-5 w-full rounded-lg border p-4 pr-12 text-sm shadow-sm focus:outline-none focus:ring-0"
                            onChange={(e) => setPassBaru(e.target.value)}
                          />

                          <span
                            onClick={togglePassword}
                            className="absolute inset-y-0 right-0 grid place-content-center px-4  focus:outline-none focus:ring-0"
                          >
                            {passwordType === "password" ? (
                              <>
                                <i className="fa-solid fa-eye-slash"></i>
                              </>
                            ) : (
                              <>
                                <i className="fa-solid fa-eye"></i>
                              </>
                            )}
                          </span>
                        </div>
                      </div>
                      <div>
                        <label htmlFor="password">Konfirmasi Password</label>
                        <div className="relative">
                          <input
                            required
                            placeholder="Konfirmasi Password Baru"
                            type={passwordType1}
                            className="md:h-5 w-full rounded-lg border p-4 pr-12 text-sm shadow-sm  focus:outline-none focus:ring-0"
                            onChange={(e) => setConPassBaru(e.target.value)}
                          />

                          <span
                            onClick={togglePassword1}
                            className="absolute inset-y-0 right-0 grid place-content-center px-4  focus:outline-none focus:ring-0"
                          >
                            {passwordType1 === "password" ? (
                              <>
                                <i className="fa-solid fa-eye-slash"></i>
                              </>
                            ) : (
                              <>
                                <i className="fa-solid fa-eye"></i>
                              </>
                            )}
                          </span>
                        </div>
                      </div>
                      <button
                        type="submit"
                        className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                      >
                        Edit
                      </button>
                    </form>
                  </div>
                </div>
              </div>
            </Modal>

              </>
            </div>
        </div>


    </div>
  )
}
