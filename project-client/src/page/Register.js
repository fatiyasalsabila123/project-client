import axios from 'axios';
import React, { useState } from 'react'
import { useHistory } from "react-router-dom";
import Swal from 'sweetalert2';

export default function Register() {
    const[username, setUserName] = useState("");
    const[email, setEmail] = useState("");
    const[password, setPassword] = useState("");
    const[role, setRole] = useState("");

    const history = useHistory();  
    const [passwordType, setPasswordType] = useState("password");
    const [passwordIcon ,setPasswordIcon] = useState("fa-solid fa-eye-slash");
  
    const togglePassword = () => {
      if (passwordType === "password") {
        setPasswordType("text");
        setPasswordIcon("fa-solid fa-eye")
        return;
      }
      setPasswordType("password");
      setPasswordIcon("fa-solid fa-eye-slash")
    };

    const register = async (e) => {
        e.preventDefault();
        try {
            await axios.post("http://localhost:1212/register/register", {
                username: username,
                email: email,
                password: password,
                role: "ADMIN",
            }) .then(() => {
                Swal.fire({
                  icon: 'success',
                  title: 'Berhasil Registrasi',
                  showConfirmButton: false,
                  timer: 1500
                })
                setTimeout(() => {
                  history.push('/')
                  window.location.reload();
                }, 1500)
              })
          } catch (error) {
            alert("Terjadi Kesalahan " + error)
          }
    }
  return (
    <div className="">
    <section class="bg-gray-50 dark:bg-gray-900">
    <div class="flex flex-col items-center justify-center px-6 py-8 mx-auto md:h-screen bg-[url('https://uideck.com/wp-content/uploads/2021/08/hero-bg-new.svg')] bg-cover lg:py-0">
        <p class="flex items-center mb-6 text-2xl font-bold text-gray-900 dark:text-white">
            SISTEM APLIKASI UKS    
        </p>
        {/* <div className='bg-blue-300 w-[30rem] absolute'>test</div> */}
        <div className='flex gap-3 bg-white w-[55rem] shadow-lg shadow-cyan-200 rounded-lg border-2 border-blue-200'>
          
        <div className='bg-blue-100 p-2 w-[30rem]'>
            <img src="https://binusasmg.sch.id/ppdb/logobinusa.png" alt="" className='w-[23rem] h-[21rem] mt-2 ml-5'/>
       </div>
        <div class="w-[40rem] relative bg-white dark:border md:mt-0 rounded-l-lg sm:max-w-md xl:p-0 dark:bg-gray-800 dark:border-gray-700">
            <div class="p-6 space-y-4 md:space-y-6 sm:p-8">
                <h1 class="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
                  Register
                </h1>
                <form class="space-y-4 md:space-y-6" action="#" onSubmit={register}>
                <div>
                        <label for="username" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">UserName</label>
                        <input type="text" name="username" id="username" placeholder="UserName" class="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required onChange={(e) => setUserName(e.target.value)}/>
                    </div>
                    <div>
                        <label for="email" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Email</label>
                        <input type="text" name="email" id="email" class="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="name@company.com" required onChange={(e) => setEmail(e.target.value)}/>
                    </div>
                    <div>
                        <label for="password" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Password</label>
                        <div className="relative">
              <input
                value={password}
                type={passwordType}
                className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="********"
                onChange={(e) => setPassword(e.target.value)}
              />

              <span
                onClick={togglePassword}
                className="absolute inset-y-0 right-0 grid place-content-center px-4"
              >
                   <i class={passwordIcon}></i>
              </span>
            </div>
                    </div>

                    <div class="flex items-center justify-between">
                        <div class="flex items-start">
                        </div>
                    </div>
                    <button type="submit" class="w-full text-white bg-primary-600 hover:bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-primary-600 dark:hover:bg-primary-700 dark:focus:ring-primary-800 bg-blue-400">Register</button>

                    <p>Jika Sudah Punya Akun Silahkan <a href="/" className='text-blue-600 '> Login</a></p>
                </form>
            </div>
        </div> 
       </div>
    </div>
  </section>
  </div>
  )
}
