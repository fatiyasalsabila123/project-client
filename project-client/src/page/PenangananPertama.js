import React, { useEffect, useState } from 'react'
import Sidebar from '../component/Sidebar'
import axios from 'axios';
import ReactPaginate from 'react-paginate';
import Swal from 'sweetalert2';
import { Modal } from 'react-bootstrap';
import { useHistory, useParams } from 'react-router-dom/cjs/react-router-dom.min';
import Navbar from '../component/Navbar';

export default function PenangananPertama() {

  const[listPenanganan, setListPenanganan] = useState([]);
  const[penanganan, setPenanganan] = useState("");
  const[namaPenanganan, setNamaPenanganan]  = useState("");
  const [pages, setPages] = useState(0);
  const [status, setStatus] = useState("");
  const [statusId, setStatusId] = useState(0);
  
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  
  const handleClose1 = () => setShow(false);
  const handleShow1 = () => setShow(true);
  const [show, setShow] = useState(false);
  const [show1, setShow1] = useState(false);
  
  const history = useHistory();
  const param = useParams();

  const getAllPenanganan = async (page = 0) => {
    await axios
      .get(`http://localhost:1212/penanganan/all-penanganan?page=${page}&userId=${localStorage.getItem("userId")}`)
      .then((res) => {
        setPages(res.data.data.totalPages);
        setListPenanganan(res.data.data.content);
        console.log(res.data.data.content);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  useEffect(() => {
    getAllPenanganan(0);
  }, []);

  const add = async (e) => {
    e.preventDefault();
    e.persist();

    try {
      await axios.post(
        `http://localhost:1212/penanganan?namaPenanganan=${namaPenanganan}&userId=${localStorage.getItem("userId")}`
      );
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "Sukses Menambahkan",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  const deletePenanganan = async (id) => {
    Swal.fire({
      title: "Apakah Anda Ingin Menghapus?",
      text: "Perubahan data tidak bisa dikembalikan!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Hapus",
      cancelButtonText: "Batal",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete("http://localhost:1212/penanganan/" + id).then((res) => {
          if (res.data.data.Hapus === true) {
            Swal.fire({
              icon: "success",
              title: "Dihapus!",
              showConfirmButton: false,
              timer: 1500,
            });

            setTimeout(() => {
              window.location.reload();
            }, 1500);
          } else {
            Swal.fire({
              icon: "error",
              title: "Data Sedang relasi",
              showConfirmButton: false,
              timer: 1500,
            });
          }
        });
      }
    });
  };

  const put = async (e) => {
    e.preventDefault();
    e.persist();

    try {
      await axios.put(
        "http://localhost:1212/penanganan/" + statusId, {
        namaPenanganan: namaPenanganan,
    });
    setShow1(false);
      Swal.fire({
        icon: "success",
        title: "Berhasil Mengedit",
        showConfirmButton: false,
        timer: 1500,
      });
    setTimeout(() => {
      window.location.reload();
    }, 1500);
    } catch (err) {
      console.log(err);
    }
  };

  const getById = async (id) => {
    await axios
      .get("http://localhost:1212/penanganan/" + id)
      .then((res) => {
        setNamaPenanganan(res.data.data.namaPenanganan)
        setStatusId(res.data.data.id);
        console.log(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi Keslahan" + error);
      });
  };


  return (
    <div className='flex'>
    <div>
        <Sidebar/>
    </div>
    <div className='overflow-x-auto h-[39rem] w-full'>
    <Navbar/>
    <div class="overflow-x-auto w-[66rem] ml-[2rem] rounded-lg border shadow-xl shadow-blue-200 border-gray-200 h-fit mt-7 bg-gradient-to-t from-rose-100 to-teal-100">
        <div className='flex text-white font-bold p-3 bg-blue-400'>
        <p className=" text-xl">Penanganan Pertama</p>
        <div className='flex ml-auto gap-4'>
        <ReactPaginate
                    className="flex gap-2 mt-1 mb-1 justify-center"
                    previousLabel={"<"}
                    nextLabel={">"}
                    breakLabel={"..."}
                    pageCount={pages}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={3}
                    onPageChange={(e) => getAllPenanganan(e.selected)}
                    containerClassName={"pagination justify-content-center"}
                    pageClassName={
                      "page-item text-sm p-1 md:h-7 h-6 text-xs px-3 border-2 hover:bg-blue-500 border-blue-200 bg-blue-600 rounded"
                    }
                    pageLinkClassName={"page-link"}
                    previousClassName={
                      "page-item p-1 px-3 md:h-7 h-6 text-xs font-medium rounded border-2 hover:bg-gray-200 hover:text-black border-gray-200"
                    }
                    previousLinkClassName={"page-link"}
                    nextClassName={
                      "page-item p-1 px-3 rounded  border-2 hover:bg-gray-200 hover:text-black border-gray-200 md:h-7 h-6 text-xs"
                    }
                    nextLinkClassName={"page-link"}
                    breakClassName={"page-item"}
                    breakLinkClassName={"page-link"}
                    activeClassName={"active"}
                  />
        <button class="bg-blue-600 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" onClick={() => setShow(true)}>
            Tambah
          </button>
          </div>
    </div>
          <table class="w-full divide-y-2 divide-gray-200 bg-white text-sm">
            <thead class="ltr:text-left rtl:text-right bg-blue-200">
              <tr>
                <th class="whitespace-nowrap px- py-2 font-medium text-gray-900">
                  No
                </th>
                <th class="whitespace-nowrap px- py-2 font-medium text-gray-900">
                  Nama Diagnosa
                </th>
                <th class="whitespace-nowrap px- py-2 font-medium text-gray-900">
                  Aksi
                </th>
              </tr>
            </thead>

            <tbody class="divide-y divide-gray-200 bg-gray-100">
              {listPenanganan.map((data, index) => {
                return (
                  <tr key={data.id}>
                  <td class="whitespace-nowrap px-4 py-2 text-center font-medium text-gray-900">
                    {index + 1}
                  </td>
                  <td class="whitespace-nowrap px-4 py-2 text-center text-gray-700">
                    {data.namaPenanganan}
                  </td>
                  <td class="whitespace-nowrap px-4 py-2 text-center text-gray-700">
                    <button>
                      <i class="fas fa-edit text-blue-500 font-bold text-lg" onClick={() => {
                            setShow1(true);
                            getById(data.id);
                          }}
></i>
                    </button>
                    <button>
                      <i class="fas fa-trash-alt ml-3 text-red-500  text-lg" onClick={() => deletePenanganan(data.id)}></i>
                    </button>
                  </td>
                </tr>)
              })}
            </tbody>
          </table>
          {/* modal add */}
          <Modal
                show={show}
                onHide={handleClose}
                id="authentication-modal"
                tabIndex="-1"
                aria-hidden="true"
                className="md:ml-[30%] ml-0 fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full"
              >
                <div className="relative w-full h-full max-w-md md:h-auto">
                  <div className="relative bg-white rounded-lg shadow dark:bg-white">
                    <button
                      type="button"
                      className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white"
                      data-modal-hide="authentication-modal"
                      onClick={handleClose}
                    >
                      <svg
                        aria-hidden="true"
                        className="w-5 h-5"
                        fill="currentColor"
                        viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          fillRule="evenodd"
                          d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                          clipRule="evenodd"
                        ></path>
                      </svg>
                      <span className="sr-only">Close modal</span>
                    </button>
                    <div className="px-6 py-6 lg:px-8">
                      <h3 className="mb-4 text-xl font-medium text-black dark:text-black">
                        Tambah Penanganan
                      </h3>
                      <form className="space-y-3" onSubmit={add}>
                        <div>
                          <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                            Nama Penanganan
                          </label>
                          <input
                            placeholder="Nama Penanganan"
                            onChange={(e) => setNamaPenanganan(e.target.value)}
                            value={namaPenanganan}
                            className="bg-white border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                            required
                          />
                        </div>
                        <button
                          type="submit"
                          className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                        >
                          Tambah
                        </button>
                      </form>
                    </div>
                  </div>
                </div>
              </Modal>   

              {/* modal edit  */}
              <Modal
                show={show1}
                onHide={!show1}
                id="authentication-modal"
                tabIndex="-1"
                aria-hidden="true"
                className="md:ml-[30%] ml-0 fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full"
              >
                <div className="relative w-full h-full max-w-md md:h-auto">
                  <div className="relative bg-white rounded-lg shadow dark:bg-white">
                    <a href='/penangananPertama'
                      type="button"
                      className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white"
                      data-modal-hide="authentication-modal"
                      onClick={() => setShow1(false)}
                    >
                      <svg
                        aria-hidden="true"
                        className="w-5 h-5"
                        fill="currentColor"
                        viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          fillRule="evenodd"
                          d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                          clipRule="evenodd"
                        ></path>
                      </svg>
                      <span className="sr-only">Close modal</span>
                    </a>
                    <div className="px-6 py-6 lg:px-8">
                      <h3 className="mb-4 text-xl font-medium text-black dark:text-black">
                        Edit Penanganan
                      </h3>
                      <form className="space-y-3" onSubmit={put}>
                        <div>
                          <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                            Nama Penanganan
                          </label>
                          <input
                            placeholder="Nama Penanganan"
                            onChange={(e) => setNamaPenanganan(e.target.value)}
                            value={namaPenanganan}
                            className="bg-white border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                            required
                          />
                        </div>
                        <button
                          type="submit"
                          className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                        >
                          Tambah
                        </button>
                      </form>
                    </div>
                  </div>
                </div>
              </Modal>   


        </div>
</div>
</div>

  )
}
