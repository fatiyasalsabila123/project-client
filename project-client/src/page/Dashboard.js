import React, { useEffect, useState } from "react";
import Sidebar from "../component/Sidebar";
import axios from "axios";
import ReactPaginate from "react-paginate";
import Navbar from "../component/Navbar";

export default function Dashboard() {
  const [namaGuru, setNamaGuru] = useState("");
  const [tempatTanggalLahir, setTempatTanggalLahir] = useState("");
  const [alamat, setAlamat] = useState("");
  const [listDaftarGuru, setListDaftarGuru] = useState([]);
  const [pages, setPages] = useState(0);
  const [kelas, setKelas] = useState("");
  const [namaSiswa, setNamaSiswa] = useState("");
  const [listDataSiswa, setListDataSiswa] = useState([]);
  const [namaKaryawan, setNamaKaryawan] = useState("");
  const [listDataKaryawan, setListDataKaryawan] = useState([]);
  const [list, setList] = useState([]);

  const getAllDaftarGuru = async (page = 0) => {
    await axios
      .get(
        `http://localhost:1212/pasien/getAll-guru?userId=${localStorage.getItem(
          "userId"
        )}`
      )
      .then((res) => {
        setListDaftarGuru(res.data.data);
        console.log(res.data.data.content);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  const getAll = async (page = 0) => {
    await axios
      .get(
        `http://localhost:1212/pasien/all-userId?page=${page}&userId=${localStorage.getItem(
          "userId"
        )}`
      )
      .then((res) => {
        setPages(res.data.data.totalPages);
        setList(res.data.data.content);
        console.log(res.data.data.content);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  useEffect(() => {
    getAllDaftarGuru(0);
    getAllDaftarSiswa(0);
    getAllKaryawan(0);
    getAll(0);
  }, []);

  const getAllKaryawan = async (page = 0) => {
    await axios
      .get(
        `http://localhost:1212/pasien/all-karyawan?userId=${localStorage.getItem(
          "userId"
        )}`
      )
      .then((res) => {
        setListDataKaryawan(res.data.data);
        console.log(res.data.data.content);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  const getAllDaftarSiswa = async (page = 0) => {
    await axios
      .get(
        `http://localhost:1212/pasien/all-siswa?&userId=${localStorage.getItem(
          "userId"
        )}`
      )
      .then((res) => {
        setListDataSiswa(res.data.data);
        console.log(res.data.data.content);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };
  return (
    <div className="flex">
      <div>
        <Sidebar />
      </div>
      <div className="overflow-x-auto w-[66rem] h-[39rem] w-full">
        <div className="flex gap-7 p-7">
          <div className="border-blue-200 shadow-blue-100 border shadow-xl w-[20rem]">
            <p className="text-lg font-semibold ml-[5rem]">
              Daftar Pasien Guru
            </p>
            <div className="flex gap-2 ml-[5rem]">
              <i class="fa-solid fa-wheelchair bg-blue-400 p-2 rounded-full text-2xl px-3 text-white"></i>
              <p className="text-3xl text-blue-400 font-semibold">
                {listDaftarGuru.length}
              </p>
              <p className="text-3xl text-blue-400 font-semibold">Guru</p>
            </div>
            <a href="/daftarGuru">
              <p className="text-center bg-blue-400 text-white mt-2">
                <i class="fa-solid fa-circle-info"></i> Detail
              </p>
            </a>
          </div>
          <div className="border-blue-200 shadow-blue-100 border shadow-lg w-[20rem]">
            <p className="text-lg font-semibold ml-[5rem]">
              Daftar Pasien Siswa
            </p>
            <div className="flex gap-2 ml-[5rem]">
              <i class="fa-solid fa-wheelchair bg-blue-400 p-2 rounded-full text-2xl px-3 text-white"></i>
              <p className="text-3xl text-blue-400 font-semibold">
                {listDataSiswa.length}
              </p>
              <p className="text-3xl text-blue-400 font-semibold">Siswa</p>
            </div>
            <a href="/daftarSiswa">
              <p className="text-center bg-blue-400 text-white mt-2">
                <i class="fa-solid fa-circle-info"></i> Detail
              </p>
            </a>
          </div>
          <div className="border-blue-200 shadow-blue-100 border shadow-lg w-[20rem]">
            <p className="text-lg font-semibold ml-[3.5rem]">
              Daftar Pasien Karyawan
            </p>
            <div className="flex gap-2 ml-[3rem]">
              <i class="fa-solid fa-wheelchair bg-blue-400 p-2 rounded-full text-2xl px-3 text-white"></i>
              <p className="text-3xl text-blue-400 font-semibold">
                {listDataKaryawan.length}
              </p>
              <p className="text-3xl text-blue-400 font-semibold">Karyawan</p>
            </div>
            <a href="/daftarKaryawan">
              <p className="text-center bg-blue-400 text-white mt-2">
                <i class="fa-solid fa-circle-info"></i> Detail
              </p>
            </a>
          </div>
        </div>
        <div>
          <div className="p-7">
            <div class="overflow-x-auto rounded-lg border border-gray-200 shadow-blue-300 shadow-xl">
              <div className="text-center text-white font-bold p-3 text-xl bg-blue-400">
                <p>Riwayat Pasien</p>
              </div>
              <table class="w-full divide-y-2 divide-gray-200 bg-white text-sm">
                <thead class="ltr:text-left rtl:text-right bg-blue-300">
                  <tr>
                    <th class="whitespace-nowrap px-4 py-2 font-medium text-gray-900">
                      No
                    </th>
                    <th class="whitespace-nowrap px-4 py-2 font-medium text-gray-900">
                      Nama Pasien
                    </th>
                    <th class="whitespace-nowrap px-4 py-2 font-medium text-gray-900">
                      Status Pasien
                    </th>
                    <th class="whitespace-nowrap px-4 py-2 font-medium text-gray-900">
                      Tanggal/Jam Periksa
                    </th>
                  </tr>
                </thead>

                <tbody class="divide-y divide-gray-200 text-center bg-gray-100">
                  {list.map((data, index) => {
                    return (
                      <tr key={data.id}>
                        <td class="whitespace-nowrap px-4 py-2 font-medium text-gray-900">
                          {index + 1}
                        </td>
                        <td class="whitespace-nowrap px-4 py-2 text-gray-700">
                          {data.username}
                        </td>
                        <td class="whitespace-nowrap px-4 py-2 text-gray-700">
                          {data.jabatan}
                        </td>
                        <td class="whitespace-nowrap px-4 py-2 text-gray-700">
                          {data.tanggalLahir} / {data.waktu}
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
              <div className="flex text-white  font-bold p-3 bg-blue-400">
                <ReactPaginate
                  className="flex gap-2 ml-[27rem] "
                  previousLabel={"<"}
                  nextLabel={">"}
                  breakLabel={"..."}
                  pageCount={pages}
                  marginPagesDisplayed={2}
                  pageRangeDisplayed={3}
                  onPageChange={(e) => getAll(e.selected)}
                  containerClassName={"pagination justify-content-center"}
                  pageClassName={
                    "page-item text-sm p-1 md:h-7 h-6 text-xs px-3 border-2 hover:bg-blue-500 border-blue-200 bg-blue-600 rounded"
                  }
                  pageLinkClassName={"page-link"}
                  previousClassName={
                    "page-item p-1 px-3 md:h-7 h-6 text-xs font-medium rounded border-2 hover:bg-gray-200 hover:text-black border-gray-200"
                  }
                  previousLinkClassName={"page-link"}
                  nextClassName={
                    "page-item p-1 px-3 rounded  border-2 hover:bg-gray-200 hover:text-black border-gray-200 md:h-7 h-6 text-xs"
                  }
                  nextLinkClassName={"page-link"}
                  breakClassName={"page-item"}
                  breakLinkClassName={"page-link"}
                  activeClassName={"active"}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
