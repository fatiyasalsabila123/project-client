import React from "react";
import Sidebar from "../component/Sidebar";
import { useState } from "react";
import { useEffect } from "react";
import axios from "axios";
import ReactPaginate from "react-paginate";
import Swal from "sweetalert2";
import { Modal } from "react-bootstrap";
import Navbar from "../component/Navbar";

export default function DaftarGuru() {
  const [username, setUsername] = useState("");
  const [jabatan, setJabatan] = useState("Guru");
  const [daftarGuru, setDaftarGuru] = useState("");
  const [listDaftarGuru, setListDaftarGuru] = useState([]);
  const [pages, setPages] = useState(0);
  const [showModal, setShowModal] = React.useState(false);
  const [tempat, setTempat] = useState("");
  const [tanggalLahir, setTanggalLahir] = useState("");
  const [alamat, setAlamat] = useState("");
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [show, setShow] = useState(false);
  const [excel, setExcel] = useState("");
  const handleShowExcel = () => setShowExcel(true);
  const [show1, setShow1] = useState(false);
  const [showExcel, setShowExcel] = useState(false);
  const handleCloseExcel = () => setShowExcel(false);

  const getAllDaftarGuru = async (page = 0) => {
    await axios
      .get(
        `http://localhost:1212/pasien/getAll-guru?userId=${localStorage.getItem("userId")}`
      )
      .then((res) => {
        setListDaftarGuru(res.data.data);
        console.log(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  useEffect(() => {
    getAllDaftarGuru(0);
  }, []);

  const add = async (e) => {
    e.preventDefault();
    e.persist();

    try {
      await axios.post(
        `http://localhost:1212/pasien/post-guru?alamat=${alamat}&jabatan=${jabatan}&tanggalLahir=${tanggalLahir}&tempat=${tempat}&userId=${localStorage.getItem("userId")}&username=${username}`
      );
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "Sukses Menambahkan",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  const deleteGuru = async (id) => {
    Swal.fire({
      title: "Apakah Anda Ingin Menghapus?",
      text: "Perubahan data tidak bisa dikembalikan!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Hapus",
      cancelButtonText: "Batal",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete("http://localhost:1212/pasien/" + id).then((res) => {
          if (res.data.data.Hapus === true) {
            Swal.fire({
              icon: "success",
              title: "Dihapus!",
              showConfirmButton: false,
              timer: 1500,
            });

            setTimeout(() => {
              window.location.reload();
            }, 1500);
          } else {
            Swal.fire({
              icon: "error",
              title: "Data Sedang relasi",
              showConfirmButton: false,
              timer: 1500,
            });
          }
        });
      }
    });
  };

  const [loading, setLoading] = useState(false);
  const downloadFormat = async () => {
    await Swal.fire({
      title: "Yakin ingin mendownload?",
      text: "Ini adalah file format excel untuk mengimport data.",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#0b409c",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, download!",
      cancelButtonText: "Batal",
    }).then((result) => {
      if (result.isConfirmed) {
        axios({
          url: `http://localhost:1212/api/excel_daftar_guru/download/templatedaftarguru
          `,
          method: "GET",
          responseType: "blob",
        }).then((response) => {
          setLoading(true);
          setTimeout(() => {
            var fileURL = window.URL.createObjectURL(new Blob([response.data]));
            var fileLink = document.createElement("a");

            fileLink.href = fileURL;
            fileLink.setAttribute("download", "format-daftarGuru.xlsx");
            document.body.appendChild(fileLink);

            fileLink.click();
            handleCloseExcel();
            setLoading(false);
          }, 2000);
        });
      }
    });
  };

  const importExcel = async (e) => {
    e.preventDefault();

    const formData = new FormData();

    formData.append("file", excel);
    formData.append("username", username);
    formData.append("tempat", tempat);
    formData.append("tanggalLahir", tanggalLahir);
    formData.append("alamat", alamat);

    await axios
      .post(
        `http://localhost:1212/api/excel_daftar_guru/upload/daftar_guru?userId=${localStorage.getItem("userId")}`,
        formData
      )
      .then(() => {
        Swal.fire({
          icon: "success",
          title: "Berhasil Ditambahkan!",
          showConfirmButton: false,
          timer: 1500,
        });

        setTimeout(() => {
          window.location.reload();
        }, 1500);
      })
      .catch((err) => {
        console.log(err);
        Swal.fire("Error", "Anda belum memilih file untuk diimport!.", "error");
      });
  };

  const download = async () => {
    await Swal.fire({
      title: "Apakah Anda Yakin?",
      text: "Data Akan Didownload!",
      icon: "question",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, Download!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios({
          url: `http://localhost:1212/api/excel_daftar_guru/download?userId=${localStorage.getItem("userId")}`,
          method: "GET",
          responseType: "blob",
        }).then((response) => {
          var fileURL = window.URL.createObjectURL(new Blob([response.data]));
          var fileLink = document.createElement("a");

          fileLink.href = fileURL;
          fileLink.setAttribute("download", "data-guru.xlsx");
          document.body.appendChild(fileLink);

          fileLink.click();
        });
        Swal.fire({
          icon: "success",
          title: "file Anda Telah Diunduh",
          showConfirmButton: false,
          timer: 1500,
        });

        setTimeout(() => {
          window.location.reload();
        }, 1500);
      }
    });
  };

  return (
    <div className="flex">
      <div>
        <Sidebar />
      </div>
      <div className="overflow-x-auto h-[39rem] w-full">
      <Navbar/>
        <div class="overflow-x-auto rounded-lg ml-[2rem] w-[66rem] h-fit mt-7 shadow-xl shadow-blue-200">
          <div className="flex justify-between text-white font-bold p-3 bg-blue-400">
            <p className=" text-xl">Daftar Guru</p>
            <div className="flex gap-5">
              <button
                class="bg-blue-600 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                onClick={handleShow}
                type="button"
              >
                Tambah
              </button>
              <button class="bg-blue-600 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" onClick={() => setShow1(true)}>
                Import Data
              </button>
              <button class="bg-blue-600 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" onClick={download}>
                Download Data
              </button>
            </div>
          </div>
          <table class="w-full divide-y-2 divide-gray-200 bg-white text-sm">
            <thead class="ltr:text-left rtl:text-right bg-blue-200">
              <tr>
                <th class="whitespace-nowrap px- py-2 font-medium text-gray-900">
                  No
                </th>
                <th class="whitespace-nowrap px- py-2 font-medium text-gray-900">
                  Nama Guru
                </th>
                <th class="whitespace-nowrap px- py-2 font-medium text-gray-900">
                  Tempat / Tanggal Lahir
                </th>
                <th class="whitespace-nowrap px- py-2 font-medium text-gray-900">
                  Alamat
                </th>
                <th class="whitespace-nowrap px- py-2 font-medium text-gray-900">
                  Aksi
                </th>
              </tr>
            </thead>

            <tbody class="divide-y divide-gray-200 bg-gray-100">
              {listDaftarGuru.map((data, index) => {
                return (
                  <tr key={data.id}>
                    <td class="whitespace-nowrap px-4 py-2 text-center font-medium text-gray-900">
                      {index + 1}
                    </td>
                    <td class="whitespace-nowrap px-4 py-2 text-center text-gray-700">
                      {data.username}
                    </td>
                    <td class="whitespace-nowrap px-4 py-2 text-center text-gray-700">
                     {data.tempat} / {data.tanggalLahir}
                    </td>
                    <td class="whitespace-nowrap px-4 py-2 text-center text-gray-700">
                      {data.alamat}
                    </td>
                    <td class="whitespace-nowrap px-4 py-2 text-center text-gray-700">
                      <a href={"/editGuru/" + data.id}>
                        <i class="fas fa-edit text-blue-500 font-bold text-lg"></i>
                      </a>
                      <button>
                        <i class="fas fa-trash-alt ml-3 text-red-500  text-lg" onClick={() => deleteGuru(data.id)}></i>
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
 {/* modal add */}
 <Modal
                show={show}
                onHide={handleClose}
                id="authentication-modal"
                tabIndex="-1"
                aria-hidden="true"
                className="md:ml-[30%] ml-0 fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full"
              >
                <div className="relative w-full h-full max-w-md md:h-auto">
                  <div className="relative bg-white rounded-lg shadow dark:bg-white">
                    <button
                      type="button"
                      className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white"
                      data-modal-hide="authentication-modal"
                      onClick={handleClose}
                    >
                      <svg
                        aria-hidden="true"
                        className="w-5 h-5"
                        fill="currentColor"
                        viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          fillRule="evenodd"
                          d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                          clipRule="evenodd"
                        ></path>
                      </svg>
                      <span className="sr-only">Close modal</span>
                    </button>
                    <div className="px-6 py-6 lg:px-8">
                      <h3 className="mb-4 text-xl font-medium text-black dark:text-black">
                        Tambah Guru
                      </h3>
                      <form className="space-y-3" onSubmit={add}>
                        <div>
                          <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                            Nama Guru
                          </label>
                          <input
                            placeholder="Nama Guru"
                            onChange={(e) => setUsername(e.target.value)}
                            value={username}
                            className="bg-white border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                            required
                          />
                        </div>
                        <div>
                          <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                            Tempat Lahir
                          </label>
                          <input
                            placeholder=" Tempat Lahir"
                            onChange={(e) => setTempat(e.target.value)}
                            value={tempat}
                            className="bg-white border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                            required
                          />
                        </div>
                        <div>
                          <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                            Tanggal Lahir
                          </label>
                          <input
                            placeholder=" Tanggal Lahir"
                            onChange={(e) => setTanggalLahir(e.target.value)}
                            value={tanggalLahir}
                            className="bg-white border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                            required
                            type="date"
                          />
                        </div>
                        
                        <div>
                          <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                            Alamat
                          </label>
                          <textarea
                            placeholder=" Alamat"
                            onChange={(e) => setAlamat(e.target.value)}
                            value={alamat}
                            className="bg-white border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                            required
                          ></textarea>
                        </div>
                        <button
                          type="submit"
                          className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                        >
                          Tambah
                        </button>
                      </form>
                    </div>
                  </div>
                </div>
              </Modal>  

                        <Modal
            show={show1}
            onHide={!show1}
            id="authentication-modal"
            tabIndex="-1"
            aria-hidden="true"
            className="md:ml-[30%] ml-0 fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full"
          >
            <div className="relative w-full h-full max-w-md md:h-auto">
              <div className="relative bg-white rounded-lg shadow dark:bg-white">
                <button
                  type="button"
                  className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white"
                  data-modal-hide="authentication-modal"
                  onClick={() => setShow1(false)}
                >
                  <svg
                    aria-hidden="true"
                    className="w-5 h-5"
                    fill="currentColor"
                    viewBox="0 0 20 20"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      fillRule="evenodd"
                      d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                      clipRule="evenodd"
                    ></path>
                  </svg>
                  <span className="sr-only">Close modal</span>
                </button>
                <div className="px-6 py-6 lg:px-8">
                  <h3 className="mb-4 text-xl font-medium text-black dark:text-black">
                    Import Data Dari Excel
                  </h3>
                  <form className="space-y-3" onSubmit={importExcel}>
                    <div className="border p-1 text-center block rounded-xl w-full">
                      <p>
                        download file dibawah untuk meninput data guru anda{" "}
                      </p>
                      <div>
                      <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" onClick={downloadFormat}>
                        Download Template Excel
                      </button>
                      </div>
                    </div>
                    <div>
                      <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                        File
                      </label>
                      <input
                        placeholder="Nama Tindakan"
                        className="bg-white border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                        required
                        autoComplete="off"
                        type="file"
                        accept=".xlsx"
                        onChange={(e) => setExcel(e.target.files[0])}
                      />
                    </div>
                    <button
                      type="submit"
                      className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                    >
                      Tambah
                    </button>
                  </form>
                </div>
              </div>
            </div>
          </Modal>
  
                  </div>
      </div>
    </div>
  );
}
