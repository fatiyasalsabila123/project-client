import React from 'react'
import Sidebar from '../component/Sidebar'
import "../css/Home.css"

export default function Home() {
  return (
    <div className='flex'>
        <div>
        <Sidebar/>
        </div>

<section
  class="overflow-hidden bg-[url('https://uideck.com/wp-content/uploads/2021/08/hero-bg-new.svg')] bg-cover sm:grid sm:grid-cols-2 sm:items-center"
>
  <div class="p-8 md:p-12 lg:px-16 lg:py-24">
    <div
      class="mx-auto max-w-xl text-center ltr:sm:text-left rtl:sm:text-right"
    >
      <h2 class="text-2xl font-bold text-gray-900 md:text-3xl bg-blue-100 p-2 rounded-3xl">
       SELAMAT DATANG
      </h2>

      <p class="hidden text-gray-500 md:mt-4 md:block">
      Nilai berhubungan dengan emosi kita, sama seperti kita mempraktikkan kebersihan fisik untuk menjaga kesehatan fisik kita, kita perlu mengamati kebersihan emosional untuk menjaga pikiran dan sikap yang sehat.
      </p>

    </div>
  </div>

  <img
    alt="Violin"
    src="https://static.vecteezy.com/system/resources/previews/005/579/671/original/online-pharmacy-app-concept-of-healthcare-drugstore-and-e-commerce-illustration-of-prescription-drugs-first-aid-kit-and-medical-supplies-being-sold-online-via-web-or-computer-technology-free-vector.jpg"
    class="h-[30rem] w-[40rem] object-cover rounded-l-3xl bg-white shadow-xl"
  />
</section>

    </div>
  )
}
