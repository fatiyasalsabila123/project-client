import React from 'react'
import { useState } from 'react'
import { useHistory, useParams } from 'react-router-dom/cjs/react-router-dom.min';
import Sidebar from '../component/Sidebar';
import axios from 'axios';
import { useEffect } from 'react';
import Swal from 'sweetalert2';

export default function EditDaftarSiswa() {
    const [show, setShow] = useState(false);
    const history = useHistory();
    const param = useParams();
    const [username, setUsername] = useState("");
    const [kelas, setKelas] = useState("");
    const [tanggalLahir, setTanggalLahir] = useState("");
    const [tempat, setTempat] = useState("");
    const [alamat, setAlamat] = useState("");
    const [jabatan, setJabatan] = useState("Siswa");


    const put = async (e) => {
        e.preventDefault();
        e.persist();
    
        try {
          await axios.put(
            `http://localhost:1212/pasien/${param.id}_siswa`, {
            username: username,
            kelas: kelas,
            tempat: tempat,
            tanggalLahir: tanggalLahir,
            alamat:alamat,
            jabatan: kelas,
        });
        setShow(false);
          Swal.fire({
            icon: "success",
            title: "Berhasil Mengedit",
            showConfirmButton: false,
            timer: 1500,
          });
        setTimeout(() => {
          history.push("/daftarSiswa");
          window.location.reload();
        }, 1500);
        } catch (err) {
          console.log(err);
        }
      };

      useEffect(() => {
        axios
          .get("http://localhost:1212/pasien/" + param.id)
          .then((response) => {
            const daftar_siswa = response.data.data;
            setUsername(daftar_siswa.username);
            setKelas(daftar_siswa.kelas);
            setTanggalLahir(daftar_siswa.tanggalLahir);
            setTempat(daftar_siswa.tempat);
            setAlamat(daftar_siswa.alamat);
          })
          .catch((error) => {
            alert("Terjadi Kesalahan " + error);
          });
      }, [param.id]);

  return (
    <div className='flex gap-[2rem]'>
    <div>
        <Sidebar/>
    </div>
    <div className='mt-[7rem]'>
        <div className='bg-blue-400 p-2 mt-8 rounded-lg'>
        <p className='text-2xl text-white font-semibold text-center'>Edit Daftar Siswa</p></div>
    <form className="space-y-3 mt-7" onSubmit={put}>
        <div className='flex gap-[5rem]'>
                    <div>
                      <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                        Nama Siswa
                      </label>
                      <input
                        placeholder="Nama Siswa"
                        className="bg-white border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-[28rem] md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                        required
                        onChange={(e) => setUsername(e.target.value)}
                        value={username}
                      />
                    </div>
                    <div>
                    <fieldset>
                        <label
                          htmlFor="frm-whatever"
                          className="md:text-base text-sm mt-1"
                        >
                          Kelas
                        </label>
                        <div className="md:relative border w-[9rem] md:w-[28rem] rounded-lg flex border-gray-300 text-gray-800 bg-white">
                          <select
                            onChange={(e) => setKelas(e.target.value)}
                            required
                            className="appearance-none py-1 px-2 w-[28rem] md:text-base text-sm h-10 bg-white"
                            name="whatever"
                            id="frm-whatever"
                          >
                            <option >Pilih</option>
                            <option value="X TKJ 1">X TKJ 1</option>
                            <option value="X TKJ 2">X TKJ 2</option>
                            <option value="X TBSM">X TBSM</option>
                            <option value="X TB">X TB</option>
                            <option value="X AKL">X AKL</option>
                            <option value="XI TKJ 1">XI TKJ 1</option>
                            <option value="XI TKJ 2">XI TKJ 2</option>
                            <option value="XI TBSM">XI TBSM</option>
                            <option value="XI TB">XI TB</option>
                            <option value="XI AKL">XI AKL</option>
                            <option value="XII TKJ 1">XII TKJ 1</option>
                            <option value="XII TKJ 2">XII TKJ 2</option>
                            <option value="XII TBSM">XII TBSM</option>
                            <option value="XII TB">XII TB</option>
                            <option value="XII AKL">XII AKL</option>
                          </select>
                          <div className="pointer-events-none md:absolute right-0 top-0 bottom-0 flex items-center px-2 text-gray-700 border-l">
                            <svg
                              className="h-4 w-4"
                              xmlns="http://www.w3.org/2000/svg"
                              viewBox="0 0 20 20"
                            >
                              <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                            </svg>
                          </div>
                        </div>
                      </fieldset>
                    </div>
                    </div>
                    <div className='flex gap-[5rem]'>
                    <div>
                      <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                        Tempat
                      </label>
                      <input
                        placeholder=" Tempat"
                        className="bg-white border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-[28rem] md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                        required
                        onChange={(e) => setTempat(e.target.value)}
                        value={tempat}
                      />
                    </div>
                    <div>
                      <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                        Tanggal Lahir
                      </label>
                      <input
                        placeholder=" Tanggal Lahir"
                        className="bg-white border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-[28rem] md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                        required
                        onChange={(e) => setTanggalLahir(e.target.value)}
                        value={tanggalLahir}
                      />
                    </div>
                    </div>
                    <div>
                      <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                        Alamat
                      </label>
                      <textarea
                        placeholder="Alamat"
                        className="bg-white border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-[28rem] md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                        required
                        onChange={(e) => setAlamat(e.target.value)}
                        value={alamat}
                      ></textarea>
                    </div>
                    <div className='flex gap-[5rem] mt-10'>
                    <a href='/daftarSiswa'
                      className="w-[28rem] text-white bg-red-400 hover:bg-red-500 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-r-lg text-sm px-5 py-2.5 text-center dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-500"
                    >
                      Batal 
                      </a>
                    <button
                      type="submit"
                      className="w-[28rem] text-white bg-blue-400 hover:bg-blue-500 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-l-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-500"
                    >
                      Simpan
                    </button>
                    </div>
                  </form>

    </div>
</div>

  )
}
