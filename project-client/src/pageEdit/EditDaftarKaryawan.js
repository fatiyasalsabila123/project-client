import React from 'react'
import { useState } from 'react'
import { useHistory, useParams } from 'react-router-dom/cjs/react-router-dom.min';
import Sidebar from '../component/Sidebar';
import { useEffect } from 'react';
import axios from 'axios';
import Swal from 'sweetalert2';

export default function EditDaftarKaryawan() {
    const [show, setShow] = useState(false);
    const history = useHistory();
    const param = useParams();
    const [username, setUsername] = useState("");
    const [tempat, setTempat] = useState("");
    const [tanggalLahir, setTanggalLahir] = useState("");
    const [alamat, setAlamat] = useState("");

    const put = async (e) => {
        e.preventDefault();
        e.persist();
    
        try {
          await axios.put(
            `http://localhost:1212/pasien/${param.id}_karyawan`, {
            username: username,
            tempat: tempat,
            tanggalLahir: tanggalLahir,
            alamat:alamat,
        });
        setShow(false);
          Swal.fire({
            icon: "success",
            title: "Berhasil Mengedit",
            showConfirmButton: false,
            timer: 1500,
          });
        setTimeout(() => {
          history.push("/daftarKaryawan");
          window.location.reload();
        }, 1500);
        } catch (err) {
          console.log(err);
        }
      };

      useEffect(() => {
        axios
          .get("http://localhost:1212/pasien/" + param.id)
          .then((response) => {
            const daftar_karyawan = response.data.data;
            setUsername(daftar_karyawan.username);
            setTempat(daftar_karyawan.tempat);
            setTanggalLahir(daftar_karyawan.tanggalLahir);
            setAlamat(daftar_karyawan.alamat);
          })
          .catch((error) => {
            alert("Terjadi Kesalahan " + error);
          });
      }, [param.id]);

  return (
    <div className='flex gap-[2rem]'>
    <div>
        <Sidebar/>
    </div>
    <div className='mt-[7rem]'>
        <div className='bg-blue-400 p-2 mt-8 rounded-lg'>
        <p className='text-2xl text-white font-semibold text-center'>Edit Daftar Karyawan</p></div>
    <form className="space-y-3 mt-7" onSubmit={put}>
        <div className='flex gap-[5rem]'>
                    <div>
                      <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                        Nama Karyawan
                      </label>
                      <input
                        placeholder="Nama Karyawan"
                        className="bg-white border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-[28rem] md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                        required
                        onChange={(e) => setUsername(e.target.value)}
                        value={username}
                      />
                    </div>
                    <div>
                      <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                        Tempat
                      </label>
                      <input
                        placeholder=" Tempat"
                        className="bg-white border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-[28rem] md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                        required
                        onChange={(e) => setTempat(e.target.value)}
                        value={tempat}
                      />
                    </div>
                    </div>
                    <div className='flex gap-[5rem]'>
                    <div>
                      <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                        Tanggal Lahir
                      </label>
                      <input
                        placeholder="Tanggal Lahir"
                        className="bg-white border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-[28rem] md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                        required
                        type = "date"
                        onChange={(e) => setTanggalLahir(e.target.value)}
                        value={tanggalLahir}
                      />
                    </div>
                    <div>
                      <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                        Alamat
                      </label>
                      <textarea
                        placeholder="Alamat"
                        className="bg-white border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-[28rem] md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                        required
                        onChange={(e) => setAlamat(e.target.value)}
                        value={alamat}
                      ></textarea>
                    </div>
                    </div>
                    <div className='flex gap-[5rem] mt-10'>
                    <a href='/daftarGuru'
                      className="w-[28rem] text-white bg-red-400 hover:bg-red-500 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-r-lg text-sm px-5 py-2.5 text-center dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-500"
                    >
                      Batal 
                      </a>
                    <button
                      type="submit"
                      className="w-[28rem] text-white bg-blue-400 hover:bg-blue-500 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-l-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-500"
                    >
                      Simpan
                    </button>
                    </div>
                  </form>

    </div>
</div>
  )
}
