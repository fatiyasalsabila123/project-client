import axios from 'axios';
import React from 'react'
import { useEffect } from 'react';
import { useState } from 'react';
import { useHistory, useParams } from 'react-router-dom/cjs/react-router-dom.min';
import Swal from 'sweetalert2';
import Sidebar from '../component/Sidebar';

export default function EditDiagnosa() {
    const[show, setShow] = useState(false);
    const history = useHistory();
    const param = useParams();
    const [namaDiagnosa, setNamaDiagnosa] = useState("");

    const put = async (e) => {
        e.preventDefault();
        e.persist();
    
        try {
          await axios.put(
            "http://localhost:1212/diagnosa/" + param.id, {
            namaDiagnosa: namaDiagnosa,
        });
        setShow(false);
          Swal.fire({
            icon: "success",
            title: "Berhasil Mengedit",
            showConfirmButton: false,
            timer: 1500,
          });
        setTimeout(() => {
          history.push("/diagnosa");
          window.location.reload();
        }, 1500);
        } catch (err) {
          console.log(err);
        }
      };

      useEffect(() => {
        axios
          .get("http://localhost:1212/diagnosa/" + param.id)
          .then((response) => {
            const diagnosa = response.data.data;
            setNamaDiagnosa(diagnosa.namaDiagnosa);
          })
          .catch((error) => {
            alert("Terjadi Kesalahan " + error);
          });
      }, [param.id]);

  return (
    <div className='flex gap-[2rem]'>
    <div>
        <Sidebar/>
    </div>
    <div className='mt-[7rem]'>
        <div className='bg-blue-400 p-2 mt-8 rounded-lg'>
        <p className='text-2xl text-white font-semibold text-center'>Edit Diagnosa</p></div>
    <form className="space-y-3 mt-7" onSubmit={put}>
        <div className='flex gap-[5rem]'>
                    <div>
                      <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                        Nama Diagnosa
                      </label>
                      <input
                        placeholder="Nama Diagnosa"
                        className="bg-white border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-[28rem] md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                        required
                        onChange={(e) => setNamaDiagnosa(e.target.value)}
                        value={namaDiagnosa}
                      />
                    </div>
                    </div>
                    <div className='flex gap-[5rem] mt-10'>
                    <a href='/daftarGuru'
                      className="w-[28rem] text-white bg-red-400 hover:bg-red-500 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-r-lg text-sm px-5 py-2.5 text-center dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-500"
                    >
                      Batal 
                      </a>
                    <button
                      type="submit"
                      className="w-[28rem] text-white bg-blue-400 hover:bg-blue-500 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-l-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-500"
                    >
                      Simpan
                    </button>
                    </div>
                  </form>

    </div>
</div>
  )
}
