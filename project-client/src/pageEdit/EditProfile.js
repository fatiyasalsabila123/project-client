import React from 'react'
import Sidebar from '../component/Sidebar'
import Swal from 'sweetalert2';
import { useState } from 'react';
import { useHistory } from 'react-router-dom/cjs/react-router-dom';
import axios from 'axios';
import { useEffect } from 'react';

export default function EditProfile() {
  
  const[alamat, setAlamat] = useState("");
  const[tanggalLahir, setTanggalLahir] = useState("");
  const history = useHistory();
  const[email, setEmail] = useState("");
  const[username, setUserName] = useState("");
  const[show, setShow] = useState(false);
  
  useEffect(() => {
    axios
      .get("http://localhost:1212/register/" + localStorage.getItem("userId"))
      .then((response) => {
        const profil = response.data.data;
        setUserName(profil.username);
        setEmail(profil.email);
        setAlamat(profil.alamat);
        setTanggalLahir(profil.tanggalLahir);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan " + error);
      });
  }, []);

  const putProfile = async (e) => {
    e.preventDefault();
    e.persist();

    try {
      await axios.put(
        `http://localhost:1212/register/` + localStorage.getItem("userId"),
        {
          username:username,
          email: email,
          alamat: alamat,
          tanggalLahir: tanggalLahir,
        }
      );
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "Berhasil Mengedit",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        history.push("/profile");
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <div className='flex gap-[2rem]'>
    <div>
        <Sidebar/>
    </div>
    <div className='mt-[7rem]'>
        <div className='bg-blue-400 p-2 mt-8 rounded-lg'>
        <p className='text-2xl text-white font-semibold text-center'>Edit Profile</p></div>
    <form className="space-y-3 mt-7" onSubmit={putProfile}>
        <div className='flex gap-[5rem]'>
                    <div>
                      <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                        UserName
                      </label>
                      <input 
                      type='text'
                      onChange={(e) => setUserName(e.target.value)}
                      value={username}
                        placeholder="UserName"
                        className="bg-white border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-[28rem] md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                        required
                      />
                    </div>
                    <div>
                    <div>
                      <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                        Email
                      </label>
                      <input
                      type='email'
                      onChange={(e) => setEmail(e.target.value)}
                      value={email}
                        placeholder="Email"
                        className="bg-white border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-[28rem] md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                        required
                      />
                    </div>
                    </div>
                    </div>
                    <div className='flex gap-[5rem]'>
                    <div>
                      <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                        Alamat
                      </label>
                      <textarea
                      type='text'
                      onChange={(e) => setAlamat(e.target.value)}
                      value={alamat}
                        placeholder=" Alamat"
                        className="bg-white border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-[28rem] md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                        required
                      ></textarea>  
                    </div>
                    <div>
                      <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                        Tanggal Lahir
                      </label>
                      <input
                      type='date'
                      onChange={(e) => setTanggalLahir(e.target.value)}
                      value={tanggalLahir}
                        placeholder="Tanggal Lahir"
                        className="bg-white border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-[28rem] md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                        required
                      />
                    </div>
                    </div>
                    <div className='flex gap-[5rem] mt-10'>
                    <a href='/profile'
                      className="w-[28rem] text-white bg-red-400 hover:bg-red-500 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-r-lg text-sm px-5 py-2.5 text-center dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-500"
                    >
                      Batal 
                      </a>
                    <button
                      type="submit"
                      className="w-[28rem] text-white bg-blue-400 hover:bg-blue-500 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-l-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-500"
                    >
                      Simpan
                    </button>
                    </div>
                  </form>

    </div>
</div>

  )
}
