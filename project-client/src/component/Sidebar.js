import axios from "axios";
import moment from "moment/moment";
import { updateLocale } from "moment/moment";
import React, { useState } from "react";
import { NavLink } from "react-router-dom/cjs/react-router-dom";
import localization from "moment/locale/id"
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import Swal from "sweetalert2";

export default function Sidebar() {

  var moment = require('moment');

  let myDate;

  myDate = moment().format('MMMM Do YYYY, h:mm:ss a');

  let tanggal = moment().locale('id');

  moment.updateLocale('id', localization);

  let time = new Date().toLocaleTimeString();
  const[currentTime, setCurrentTime] = useState(time);

  const updateTime = () => {
    let time = new Date().toLocaleTimeString();
    setCurrentTime(time);
  }

  setInterval(updateTime, 1000)

  const history = useHistory();

  const logout = () => {
    Swal.fire({
      title: "Anda Yakin Ingin Logout",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya",
      cancelButtonText: "Batal",
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          icon: "success",
          title: "Berhasil Logout",
          showConfirmButton: false,
          timer: 1500,
        });
        //Untuk munuju page selanjutnya
        history.push("/");
        setTimeout(() => {
          window.location.reload();
        }, 1500);
        localStorage.clear();
      }
    });
  };
  return (
    <div class="flex h-screen flex-col shadow-lg shadow-blue-400 overflow-x-auto h-[39rem] justify-between w-[15rem] border-e bg-white">
      <div class="px-3 py-2">
        <span class="ml-5 font-semibold">SISTEM APLIKASI UKS</span>
        <p className="ml-3 font-semibold">SMK BINUS SEMARANG</p>

        <nav aria-label="Main Nav" class="mt-6 flex flex-col space-y-1">
          <a
            href="/dashboard"
            class="flex items-center gap-2 shadow-md shadow-blue-300 rounded-lg bg-blue-400 px-4 py-2 text-white"
          >
            <i class="fas fa-home"></i>

            <span class="text-sm font-medium"> Dashboard </span>
          </a>

          <a
            href="/periksaPasien"
            class="flex items-center gap-2 shadow-md shadow-blue-300 rounded-lg px-4 py-2 text-gray-500 hover:bg-blue-400 hover:text-white"
          >
            <i class="fas fa-stethoscope"></i>

            <span class="text-sm font-medium"> Periksa Pasien </span>
          </a>

          <details class="group [&_summary::-webkit-details-marker]:hidden">
            <summary class="flex cursor-pointer items-center shadow-md shadow-blue-300 justify-between rounded-lg px-4 py-2 text-gray-500 hover:bg-blue-400 hover:text-white">
              <div class="flex items-center gap-2">
                <i class="fas fa-database"></i>

                <span class="text-sm font-medium"> Data </span>
              </div>

              <span class="shrink-0 transition duration-300 group-open:-rotate-180">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  class="h-5 w-5"
                  viewBox="0 0 20 20"
                  fill="currentColor"
                >
                  <path
                    fill-rule="evenodd"
                    d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                    clip-rule="evenodd"
                  />
                </svg>
              </span>
            </summary>

            <nav aria-label="Teams Nav" class="mt-2 flex flex-col px-4">
              <a
                href="/daftarGuru"
                class="flex items-center shadow-md shadow-blue-300 gap-2 rounded-lg px-4 py-2 text-gray-500 hover:bg-blue-400 hover:text-white"
              >
                <i class="fas fa-user-graduate"></i>

                <span class="text-sm font-medium"> Daftar Guru </span>
              </a>

              <a
                href="/daftarSiswa"
                class="flex items-center gap-2 shadow-md shadow-blue-300 rounded-lg px-4 py-2 text-gray-500 hover:bg-blue-400 hover:text-white"
              >
                <i class="fas fa-user"></i>

                <span class="text-sm font-medium"> Daftar Siswa </span>
              </a>

              <a
                href="/daftarKaryawan"
                class="flex items-center gap-2 shadow-md shadow-blue-300 rounded-lg px-4 py-2 text-gray-500 hover:bg-blue-400 hover:text-white"
              >
                <i class="fas fa-users"></i>

                <span class="text-sm font-medium"> Daftar Karyawan </span>
              </a>
            </nav>
          </details>

          <a
            href="/diagnosa"
            class="flex items-center gap-2 shadow-md shadow-blue-300 rounded-lg px-4 py-2 text-gray-500 hover:bg-blue-400 hover:text-white"
          >
            <i class="fas fa-diagnoses"></i>

            <span class="text-sm font-medium"> Diagnosa </span>
          </a>

          <a
            href="/penangananPertama"
            class="flex items-center gap-2 shadow-md shadow-blue-300 rounded-lg px-4 py-2 text-gray-500 hover:bg-blue-400 hover:text-white"
          >
            <i class="fas fa-heartbeat"></i>

            <span class="text-sm font-medium"> Penanganan Pertama </span>
          </a>

          <a
            href="/tindakan"
            class="flex items-center gap-2 shadow-md shadow-blue-300 rounded-lg px-4 py-2 text-gray-500 hover:bg-blue-400 hover:text-white"
          >
            <i class="fas fa-procedures"></i>

            <span class="text-sm font-medium"> Tindakan </span>
          </a>

          <a
            href="/daftarObat"
            class="flex items-center gap-2 shadow-md shadow-blue-300 rounded-lg px-4 py-2 text-gray-500 hover:bg-blue-400 hover:text-white"
          >
            <i class="fas fa-medkit"></i>

            <span class="text-sm font-medium"> Daftar Obat P3K </span>
          </a>
        </nav>
      </div>
      
      
      <div className="text-center font-bold mt-2">
        <p>{tanggal.format('MMMM Do YYYY')}</p>
        <p>{currentTime}</p>
        </div>

      

      <div class="ml-6 sticky inset-x-0 bottom-10 top-[36rem] border-t border-gray-100">
        <button onClick={logout}
          class="flex items-center gap-2 rounded-lg px-4 py-2 text-gray-500 hover:text-gray-700"
        >
          <i class="fas fa-sign-out-alt"></i>

          <span class="text-sm font-medium"> Logout </span>
        </button>
      </div>
    </div>
  );
}
