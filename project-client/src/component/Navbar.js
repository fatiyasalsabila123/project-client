import axios from 'axios';
import React from 'react'
import { useEffect } from 'react';
import { useState } from 'react'

export default function Navbar() {
  const[username, setUserName] = useState("");
  const[email, setEmail] = useState("");
  const[foto, setFoto] = useState(null);
  const[profile, setProfile] = useState({
    username:"",
    email:"",
    foto: null,
  });

  const getAll = async () => {
    await axios
      .get("http://localhost:1212/register/" + localStorage.getItem("userId"))
      .then((res) => {
        setProfile(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  useEffect(() => {
    getAll();
  }, []);
  return (
    // navbar
    <div>
  <div class="max-w-screen-xl border-b border-gray-100">
 <div class="sticky inset-x-0">
    <a href="/profile" class="flex float-right gap-2 p-4 text-right">

      <div>
        <p class="text-xs">
          <strong class="block font-medium">{profile.username}</strong>

          <span> {profile.email}</span>
        </p>
      </div>
      {foto ? (
      <img
        alt="Man"
        src={profile.foto}
        class="h-10 w-10 rounded-full object-cover"
      />
      ):(
        <img
        alt="Man"
        src="https://cdna.artstation.com/p/assets/images/images/034/457/364/large/shin-min-jeong-.jpg?1612345089"
        class="h-10 w-10 rounded-full object-cover"
      />
      )}
    </a>
  </div>  
  </div>
    </div>
  )
}
