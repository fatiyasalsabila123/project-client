import { BrowserRouter, Route, Switch } from "react-router-dom";
import Login from "./page/Login";
import Sidebar from "./component/Sidebar";
import Home from "./page/Home";
import Diagnosa from "./page/Diagnosa";
import PenangananPertama from "./page/PenangananPertama";
import Tindakan from "./page/Tindakan";
import DaftarObat from "./page/DaftarObat";
import Dashboard from "./page/Dashboard";
import { Router } from "react-router-dom/cjs/react-router-dom.min";
import Register from "./page/Register";
import DaftarGuru from "./page/DaftarGuru";
import DaftarSiswa from "./page/DaftarSiswa";
import DaftarKaryawan from "./page/DaftarKaryawan";
import PeriksaPasien from "./page/PeriksaPasien";
import DaftarGuruEdit from "./pageEdit/DaftarGuruEdit";
import EditDaftarSiswa from "./pageEdit/EditDaftarSiswa";
import EditDaftarKaryawan from "./pageEdit/EditDaftarKaryawan";
import EditDiagnosa from "./pageEdit/EditDiagnosa";
import Profile from "./page/Profile";
import EditProfile from "./pageEdit/EditProfile";
import TanganiPasien from "./page/TanganiPasien";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
      <Switch>
        <Route path="/" component={Login} exact/>
        <Route path="/sidebar" component={Sidebar} exact/>
        <Route path="/home" component={Home} exact/>
        <Route path="/diagnosa" component={Diagnosa} exact/>
        <Route path="/penangananPertama" component={PenangananPertama} exact/>
        <Route path="/tindakan" component={Tindakan} exact/>
        <Route path="/daftarObat" component={DaftarObat} exact/>
        <Route path="/dashboard" component={Dashboard} exact/>
        <Route path="/register" component={Register} exact/>
        <Route path="/daftarGuru" component={DaftarGuru} exact/>
        <Route path="/daftarSiswa" component={DaftarSiswa} exact/>
        <Route path="/daftarKaryawan" component={DaftarKaryawan} exact/>
        <Route path="/periksaPasien" component={PeriksaPasien} exact/>
        <Route path="/profile" component={Profile} exact/>
        <Route path="/tanganiPasien/:id" component={TanganiPasien} exact/>
        <Route path="/editGuru/:id" component={DaftarGuruEdit} exact/>
        <Route path="/editSiswa/:id" component={EditDaftarSiswa} exact/>
        <Route path="/editKaryawan/:id" component={EditDaftarKaryawan} exact/>
        <Route path="/editDiagnosa/:id" component={EditDiagnosa} exact/>
        <Route path="/editProfile/:id" component={EditProfile} exact/>
        
        </Switch>
        </BrowserRouter>
    </div>
  );
}

export default App;
