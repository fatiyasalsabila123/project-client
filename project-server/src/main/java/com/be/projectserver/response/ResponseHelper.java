package com.be.projectserver.response;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ResponseHelper {
    public static <T> CommoneResponse<T> ok(T data) {
        CommoneResponse<T> response = new CommoneResponse<>();
        response.setMessage("message");
        response.setStatus("200");
        response.setData(data);
        return response;
    }

    public static <T>ResponseEntity<CommoneResponse<T>> error(String error, HttpStatus httpStatus) {
        CommoneResponse<T> response = new CommoneResponse<>();
        response.setStatus(httpStatus.name());
        response.setMessage(httpStatus.name());
        response.setData((T) error);
        return new ResponseEntity<>(response, httpStatus);
    }
}
