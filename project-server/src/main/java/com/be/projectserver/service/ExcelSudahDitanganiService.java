package com.be.projectserver.service;

import java.io.ByteArrayInputStream;

public interface ExcelSudahDitanganiService {

    ByteArrayInputStream findByIdPasien(Long id);
}
