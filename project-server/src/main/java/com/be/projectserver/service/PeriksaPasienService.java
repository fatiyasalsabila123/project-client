package com.be.projectserver.service;

import com.be.projectserver.dto.PeriksaPasienDto;
import com.be.projectserver.dto.TanganiDto;
import com.be.projectserver.model.PeriksaPasien;
import org.springframework.data.domain.Page;

import java.io.ByteArrayInputStream;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public interface PeriksaPasienService {

    Page<PeriksaPasien> allPasien(Long userId, Long page);

    PeriksaPasien addPasien(PeriksaPasienDto periksaPasien);

    PeriksaPasien editTangani(Long id, TanganiDto tanganiDto);

    PeriksaPasien getId(Long id);

    List<PeriksaPasien> filterTanggal(String starttanggal, String endtanggal);

}
