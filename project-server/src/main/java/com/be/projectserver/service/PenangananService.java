package com.be.projectserver.service;

import com.be.projectserver.dto.PenangananDto;
import com.be.projectserver.model.DaftarObat;
import com.be.projectserver.model.Penanganan;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

public interface PenangananService {

    Page<Penanganan> getAll (String query, Long page);
    Penanganan addPenanganan (PenangananDto penanganan);
    Penanganan editPenanganan (Long id, Penanganan penanganan);
    Penanganan getId (Long id);
    Map<String, Boolean> deleteById(Long id);
    Page<Penanganan> allPenanganan(Long page, Long userId);
}
