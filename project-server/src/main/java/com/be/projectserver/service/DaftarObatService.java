package com.be.projectserver.service;

import com.be.projectserver.dto.DaftarObatDto;
import com.be.projectserver.model.DaftarObat;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

public interface DaftarObatService {

    Page<DaftarObat> getAll (String query, Long page);
    DaftarObat addDaftarObat (DaftarObatDto daftarObat);
    DaftarObat editDaftarObat(Long id, DaftarObat daftarObat);
    DaftarObat getIdDaftarObat (Long id);
    Map<String, Boolean> deleteById(Long id);
    Page<DaftarObat> allDaftarObat(Long page, Long userId);

}
