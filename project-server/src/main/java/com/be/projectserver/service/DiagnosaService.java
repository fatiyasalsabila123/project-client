package com.be.projectserver.service;

import com.be.projectserver.dto.DiagnosaDto;
import com.be.projectserver.model.DaftarObat;
import com.be.projectserver.model.Diagnosa;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

public interface DiagnosaService {

    Page<Diagnosa> getAllDiagnosa (String query, Long page);

    Diagnosa addDiagnosa (DiagnosaDto diagnosa);

    Diagnosa getId (Long id);

    Diagnosa editDiagnosa (Long id, Diagnosa diagnosa);

    Map<String, Boolean> deleteDiagnosa (Long id);

    Page<Diagnosa> allDiangnosa(Long page, Long userId);
}
