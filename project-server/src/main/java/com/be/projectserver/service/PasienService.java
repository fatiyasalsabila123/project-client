package com.be.projectserver.service;

import com.be.projectserver.dto.PasienDto;
import com.be.projectserver.dto.PasienGuruDto;
import com.be.projectserver.dto.PasienKaryawanDto;
import com.be.projectserver.dto.PasienSiswaDto;
import com.be.projectserver.model.Pasien;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface PasienService {


    List<Pasien> allGuru (Long userId);

    List<Pasien> allSiswa (Long userId);

    List<Pasien> allKaryawan (Long userId);

    Page<Pasien> allSiswaId(Long page, Long userId);

    Pasien addPasien (PasienDto pasienDto);

    Pasien addSiswa (PasienSiswaDto pasienDto);

    Pasien addKaryawan (PasienDto pasienDto);

    Pasien editPasien (Long id, PasienDto pasienDto);

    Pasien editSiswa (Long id, PasienSiswaDto pasienDto);

    Pasien editKaryawan (Long id, PasienDto pasienDto);

    Pasien getId (Long id);

    Map<String, Boolean> deleteById (Long id);

}
