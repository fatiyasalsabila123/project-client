package com.be.projectserver.service;

import com.be.projectserver.dto.TindakanDto;
import com.be.projectserver.model.DaftarObat;
import com.be.projectserver.model.Tindakan;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

public interface TindakanService {

    Page<Tindakan> getAll (String query, Long page);
    Tindakan addTindakan (TindakanDto tindakan);
    Tindakan editTindakan (Long id, Tindakan tindakan);
    Page<Tindakan> allTindakan(Long page, Long userId);;
    Map<String, Boolean> deleteById(Long id);
    Tindakan getIdTindakan (Long id);

}
