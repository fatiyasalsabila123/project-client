package com.be.projectserver.controler;

import com.be.projectserver.dto.ExcelDto;
import com.be.projectserver.impl.ExcelDaftarGuruImpl;
import com.be.projectserver.impl.TemplateDaftarGuruImpl;
import com.be.projectserver.model.ExcelDaftarGuru;
import com.be.projectserver.model.ResponseMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/excel_daftar_guru")
public class ExcelDaftarGuruControler {


    @Autowired
    TemplateDaftarGuruImpl templateDaftarGuru;

    @Autowired
    ExcelDaftarGuruImpl excelDaftarGuru;

    @GetMapping("/download/templatedaftarguru")
    public ResponseEntity<Resource> getFileTemplate() {
        String filename = "template_daftar_guru.xlsx";
        InputStreamResource file = new InputStreamResource(templateDaftarGuru.pExcell());

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body(file);
    }

    @GetMapping("/download")
    public ResponseEntity<Resource> getFileSiswa(ExcelDto excelDto, Long userId) {
        String filename = "daftar_guru.xlsx";
        InputStreamResource file = new InputStreamResource(excelDaftarGuru.loadGuru(excelDto, userId));

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body  (file);
    }

    @PostMapping(path = "/upload/daftar_guru")
    public ResponseEntity<ResponseMessage> uploadFileSiswa(@RequestPart("file") MultipartFile file, ExcelDto excelDto) {
        String message = "";
        if (ExcelDaftarGuru.hasExcelFormat(file)) {
            try {
                excelDaftarGuru.saveDaftarGuru(file, excelDto);
                message = "Uploaded the file successfully: " + file.getOriginalFilename();
                return ResponseEntity.status(HttpStatus.OK).body (new ResponseMessage(message));

            } catch (Exception e) {
                System.out.println(e);
                message = "Could not upload the file: " + file.getOriginalFilename() + "!";
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
            }
        }
        message = "Please upload an excel file!";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage(message));
    }
}
