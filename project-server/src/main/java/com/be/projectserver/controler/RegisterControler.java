package com.be.projectserver.controler;

import com.be.projectserver.response.CommoneResponse;
import com.be.projectserver.response.ResponseHelper;
import com.be.projectserver.user.Dto.RegisterDto;
import com.be.projectserver.user.model.*;
import com.be.projectserver.user.service.RegisterService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

@RestController
@RequestMapping("/register")
@CrossOrigin(origins = "http://localhost:3000")
public class RegisterControler {

    @Autowired
    private RegisterService registerService;

    @Autowired
    private ModelMapper modelMapper;

    @PostMapping("/register")
    public CommoneResponse<Register> register (@RequestBody Register register) {
        return ResponseHelper.ok(registerService.register(register));
    }

    @PostMapping("/login")
    public CommoneResponse<Map<String, Object>> login(@RequestBody Login login) {
        return ResponseHelper.ok(registerService.login(login));
    }

    @GetMapping("/{id}")
    public CommoneResponse<Register> getById(@PathVariable("id") Long id) {
        return ResponseHelper.ok(registerService.getById(id));
    }

    @PutMapping(path = "/{id}")
    public CommoneResponse<Register> update(@PathVariable("id") Long id,@RequestBody Update update) {
        return ResponseHelper.ok(registerService.update(id, modelMapper.map(update , Register.class)));
    }

    @PutMapping(path = "/password/{id}")
    public CommoneResponse<Register> updatePassword(@PathVariable("id") Long id,@RequestBody Password password) {
        return ResponseHelper.ok(registerService.updatePAssword(id, modelMapper.map(password , Register.class)));
    }

    @PutMapping(path = "/foto/{id}", consumes = "multipart/form-data")
    public CommoneResponse<Register> updateFoto(@PathVariable("id") Long id, Foto foto, @RequestPart("file") MultipartFile multipartFile) {
        return ResponseHelper.ok(registerService.updateFoto(id, modelMapper.map(foto , Register.class) , multipartFile));
    }

    @PutMapping(path = "/About/{id}")
    public CommoneResponse<Register> updateAbout(@PathVariable("id") Long id, AboutDto aboutDto) {
        return ResponseHelper.ok(registerService.updateAbout(id, modelMapper.map(aboutDto , AboutDto.class)));
    }
    @PutMapping(path = "/Text/{id}")
    public CommoneResponse<Register> updateText(@PathVariable("id") Long id, TextDto textDto) {
        return ResponseHelper.ok(registerService.updateText(id, modelMapper.map(textDto , TextDto.class)));
    }


    @DeleteMapping("/{id}")
    public CommoneResponse<Map<String, Boolean>> deleteSekolah(@PathVariable("id") Long id) {
        return ResponseHelper.ok(registerService.deleteSekolah(id));
    }
    @GetMapping
    public CommoneResponse<Page<Register>> getAll(@RequestParam(name = "query", required = false) String query, Long page) {
        return ResponseHelper.ok(registerService.getAll(query == null ? "" : query, page));
    }
}
