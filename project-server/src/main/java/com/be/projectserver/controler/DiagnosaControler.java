package com.be.projectserver.controler;

import com.be.projectserver.dto.DiagnosaDto;
import com.be.projectserver.model.DaftarObat;
import com.be.projectserver.model.Diagnosa;
import com.be.projectserver.service.DiagnosaService;
import com.be.projectserver.response.CommoneResponse;
import com.be.projectserver.response.ResponseHelper;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/diagnosa")
@CrossOrigin("http://localhost:3000")
public class DiagnosaControler {

    @Autowired
    DiagnosaService diagnosaService;

    @Autowired
    ModelMapper modelMapper;

    @GetMapping("all") //unruk melihat semua data dan bisa mencari data
    public CommoneResponse<Page<Diagnosa>> getAllGuru(@RequestParam(name = "query", required = false) String query, @RequestParam(name = "page")Long page) {
        return ResponseHelper.ok(diagnosaService.getAllDiagnosa(query == null ? "": query, page));
    }

    @GetMapping("/{id}") // melihat data sesuai id
    public CommoneResponse<Diagnosa> getId (@PathVariable("id") Long id) {
        return ResponseHelper.ok(diagnosaService.getId(id));
    }

    @PostMapping //untuk menambahkan data
    public CommoneResponse<Diagnosa> addDiagnosa (DiagnosaDto diagnosa) {
        return ResponseHelper.ok(diagnosaService.addDiagnosa(modelMapper.map(diagnosa, DiagnosaDto.class)));
    }

    @PutMapping("/{id}") //untuk mengedit data per id
    public CommoneResponse<Diagnosa> editDiagnosa (@PathVariable("id") Long id, @RequestBody Diagnosa diagnosa) {
        return ResponseHelper.ok(diagnosaService.editDiagnosa(id, diagnosa));
    }

    @DeleteMapping("/{id}")// untuk deleete per id
    public CommoneResponse<?> deleteDiagnosa (@PathVariable("id") Long id) {
        return ResponseHelper.ok(diagnosaService.deleteDiagnosa(id));
    }

    @GetMapping("/all-diagnosa")// untuk menampilkan semua data
    public CommoneResponse<Page<Diagnosa>> allDiagnosa(@RequestParam (name = "userId") Long userId, @RequestParam(name = "page") Long page) {
        return ResponseHelper.ok(diagnosaService.allDiangnosa(page, userId));
    }
}
