package com.be.projectserver.controler;

import com.be.projectserver.dto.PeriksaPasienDto;
import com.be.projectserver.dto.TanganiDto;
import com.be.projectserver.impl.ExcelSudahDitanganiImpl;
import com.be.projectserver.impl.RekapDataImpl;
import com.be.projectserver.model.PeriksaPasien;
import com.be.projectserver.response.CommoneResponse;
import com.be.projectserver.response.ResponseHelper;
import com.be.projectserver.service.PeriksaPasienService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/periksa_pasien")
public class PeriksaPasienControler {

    @Autowired
    PeriksaPasienService periksaPasienService;

    @Autowired
    ExcelSudahDitanganiImpl excelSudahDitangani;


    @Autowired
    RekapDataImpl rekapData;

    @GetMapping("/download/rekap_data")
    public ResponseEntity<Resource> getFileRekapData(String starttanggal, String endtanggal) {
        String filename = "rekap_data.xlsx";
        InputStreamResource file = new InputStreamResource(rekapData.loadRekapData(starttanggal, endtanggal));

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body  (file);
    }

    @GetMapping("/downloadDataPasienSudahDiTangani")
    public ResponseEntity<Resource> getFilePasien(Long id) {
        String filename = "daftarPasien_SudahDitangani.xlsx";
        InputStreamResource file = new InputStreamResource(excelSudahDitangani.findByIdPasien(id));

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body(file);
    }

    @GetMapping
    public CommoneResponse<Page<PeriksaPasien>> allPasien(Long userId, @RequestParam(name = "page")Long page){
        return ResponseHelper.ok(periksaPasienService.allPasien(userId, page));
    }

    @GetMapping("/filterTanggal")
    public CommoneResponse<List<PeriksaPasien>> filterTanggal(String starttanggal, String endtanggal){
        return ResponseHelper.ok(periksaPasienService.filterTanggal(starttanggal, endtanggal));
    }

    @PostMapping
    public CommoneResponse<PeriksaPasien> addPasien( PeriksaPasienDto siswa ){
        return ResponseHelper.ok(periksaPasienService.addPasien(siswa));
    }

    @PutMapping("/{id}") //untuk mengedit data per id
    public CommoneResponse<PeriksaPasien> editTangani (@PathVariable("id") Long id,TanganiDto tanganiDto) {
        return ResponseHelper.ok(periksaPasienService.editTangani(id, tanganiDto));
    }

    @GetMapping("/{id}")
    public CommoneResponse <PeriksaPasien> getId(@PathVariable("id")Long id){
        return ResponseHelper.ok( periksaPasienService.getId(id));
    }
}
