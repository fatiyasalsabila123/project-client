package com.be.projectserver.controler;

import com.be.projectserver.dto.PenangananDto;
import com.be.projectserver.model.DaftarObat;
import com.be.projectserver.model.Penanganan;
import com.be.projectserver.service.PenangananService;
import com.be.projectserver.response.CommoneResponse;
import com.be.projectserver.response.ResponseHelper;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/penanganan")
@CrossOrigin("http://localhost:3000")
public class PenangananControler {

    @Autowired
    PenangananService penangananService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("all")
    public CommoneResponse<Page<Penanganan>> getAllPenanganan(@RequestParam(required = false)String search, @RequestParam(name = "page")Long page){
        return ResponseHelper.ok(penangananService.getAll(search == null ? "": search, page));

    }
    @DeleteMapping("/{id}")
    public CommoneResponse<?> deleteById(@PathVariable("id") Long id) {
        return ResponseHelper.ok( penangananService.deleteById(id));
    }

    @GetMapping("/{id}")
    public CommoneResponse <Penanganan> getId(@PathVariable("id")Long id){
        return ResponseHelper.ok( penangananService.getId(id));
    }
    @PostMapping
    public CommoneResponse <Penanganan> addKelas(PenangananDto penanganan) {
        return ResponseHelper.ok( penangananService.addPenanganan(penanganan)) ;
    }
    @PutMapping(path = "/{id}")
    public CommoneResponse <Penanganan> editPenanganan(@PathVariable("id") Long id,@RequestBody Penanganan penanganan) {
        return ResponseHelper.ok( penangananService.editPenanganan(id, penanganan));
    }
    @GetMapping("/all-penanganan")
    public CommoneResponse<Page<Penanganan>> allPenanganan(@RequestParam (name = "userId") Long userId, @RequestParam(name = "page") Long page) {
        return ResponseHelper.ok(penangananService.allPenanganan(page, userId));
    }
}
