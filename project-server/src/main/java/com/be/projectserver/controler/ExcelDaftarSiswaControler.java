package com.be.projectserver.controler;

import com.be.projectserver.dto.ExcelDto;
import com.be.projectserver.impl.ExcelDaftarSiswaImpl;
import com.be.projectserver.model.ExcelDaftarSiswa;
import com.be.projectserver.model.ResponseMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("api/excel_daftar_siswa")
public class ExcelDaftarSiswaControler {


    @Autowired
    ExcelDaftarSiswaImpl excelDaftarSiswa;

    @GetMapping("/download/templatedaftarsiswa")
    public ResponseEntity<Resource> getFileTemplate() {
        String filename = "template_daftar_siswa.xlsx";
        InputStreamResource file = new InputStreamResource(excelDaftarSiswa.pExcell());

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body(file);
    }

    @GetMapping("/download")
    public ResponseEntity<Resource> getFileSiswa(ExcelDto excelDto, Long userId) {
        String filename = "daftar_siswa.xlsx";
        InputStreamResource file = new InputStreamResource(excelDaftarSiswa.loadSiswa(excelDto, userId));

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body(file);
    }

    @PostMapping(path = "/upload/daftar_siswa")
    public ResponseEntity<ResponseMessage> uploadFileSiswa(@RequestPart("file") MultipartFile file, ExcelDto exelDto) {
        String message = "";
        if (ExcelDaftarSiswa.hasExcelFormat(file)) {
            try {
                excelDaftarSiswa.saveDaftarSiswa(file, exelDto);
                message = "Uploaded the file successfully: " + file.getOriginalFilename();
                return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));

            } catch (Exception e) {
                System.out.println(e);
                message = "Could not upload the file: " + file.getOriginalFilename() + "!";
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
            }
        }
        message = "Please upload an excel file!";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage(message));
    }
}
