package com.be.projectserver.controler;

import com.be.projectserver.dto.TindakanDto;
import com.be.projectserver.model.DaftarObat;
import com.be.projectserver.response.CommoneResponse;
import com.be.projectserver.response.ResponseHelper;
import com.be.projectserver.model.Tindakan;
import com.be.projectserver.service.TindakanService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/tindakan")
@CrossOrigin("http://localhost:3000")
public class TindakanControler {

    @Autowired
    TindakanService tindakanService;

    @Autowired
    ModelMapper modelMapper;

    @GetMapping("/all") //untuk melihat semua data
    public CommoneResponse<Page<Tindakan>> getAll(@RequestParam(name = "query", required = false) String query, @RequestParam(name = "page")Long page) {
        return ResponseHelper.ok(tindakanService.getAll(query == null ? "": query, page));
    }

    @GetMapping("/{id}") //untuk melihat sesaui id
    public CommoneResponse<Tindakan> getIdTindakan(@PathVariable("id")Long id) {
        return ResponseHelper.ok(tindakanService.getIdTindakan(id)) ;
    }

    @PostMapping // untuk mengepost data / untuk register
    public CommoneResponse<Tindakan> addTindakan( TindakanDto tindakan) {
        return ResponseHelper.ok(tindakanService.addTindakan(modelMapper.map(tindakan,TindakanDto.class)));
    }

    @PutMapping("/{id}") // untuk mengedit data sesuai id
    public CommoneResponse<Tindakan> editTindakan(@PathVariable("id") Long id, @RequestBody Tindakan tindakan) {
        return ResponseHelper.ok(tindakanService.editTindakan(id, tindakan));
    }

    @DeleteMapping("/{id}") // untuk menghapus data sesuai id
    public CommoneResponse <?> deleteById(@PathVariable("id") Long id) {
        return ResponseHelper.ok(tindakanService.deleteById(id));}

    @GetMapping("/all-Tindakan")
    public CommoneResponse<Page<Tindakan>> allTindakan(@RequestParam (name = "userId") Long userId, @RequestParam(name = "page") Long page) {
        return ResponseHelper.ok(tindakanService.allTindakan(page, userId));
    }
}
