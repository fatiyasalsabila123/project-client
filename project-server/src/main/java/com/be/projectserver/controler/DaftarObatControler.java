package com.be.projectserver.controler;

import com.be.projectserver.dto.DaftarObatDto;
import com.be.projectserver.model.DaftarObat;
import com.be.projectserver.model.Diagnosa;
import com.be.projectserver.response.CommoneResponse;
import com.be.projectserver.response.ResponseHelper;
import com.be.projectserver.service.DaftarObatService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/daftar_obat")
@CrossOrigin("http://localhost:3000")
public class DaftarObatControler {

    @Autowired
    DaftarObatService daftarObatService;

    @Autowired
    ModelMapper modelMapper;

    @GetMapping("all") //unruk melihat semua data dan bisa mencari data
    public CommoneResponse<Page<DaftarObat>> getAll(@RequestParam(name = "query", required = false) String query, @RequestParam(name = "page")Long page) {
        return ResponseHelper.ok(daftarObatService.getAll(query == null ? "": query, page));
    }

    @GetMapping("/{id}") // melihat data sesuai id
    public CommoneResponse<DaftarObat> getIdDaftarObat (@PathVariable("id") Long id) {
        return ResponseHelper.ok(daftarObatService.getIdDaftarObat(id));
    }

    @PostMapping //untuk menambahkan data
    public CommoneResponse<DaftarObat> addDaftarObat (DaftarObatDto diagnosa) {
        return ResponseHelper.ok(daftarObatService.addDaftarObat(modelMapper.map(diagnosa, DaftarObatDto.class)));
    }

    @PutMapping("/{id}") //untuk mengedit data per id
    public CommoneResponse<DaftarObat> editDaftarObat (@PathVariable("id") Long id, @RequestBody DaftarObat diagnosa) {
        return ResponseHelper.ok(daftarObatService.editDaftarObat(id, diagnosa));
    }

    @DeleteMapping("/{id}")// untuk deleete per id
    public CommoneResponse<?> deleteById (@PathVariable("id") Long id) {
        return ResponseHelper.ok(daftarObatService.deleteById(id));
    }

    @GetMapping("/all-daftar_obat")// untuk menampilkan semua data
    public CommoneResponse<Page<DaftarObat>> allDaftarObat(@RequestParam (name = "userId") Long userId, @RequestParam(name = "page") Long page) {
        return ResponseHelper.ok(daftarObatService.allDaftarObat(page, userId));
    }
}
