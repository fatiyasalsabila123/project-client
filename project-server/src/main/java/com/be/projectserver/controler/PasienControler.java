package com.be.projectserver.controler;

import com.be.projectserver.dto.*;
import com.be.projectserver.model.Pasien;
import com.be.projectserver.response.CommoneResponse;
import com.be.projectserver.response.ResponseHelper;
import com.be.projectserver.service.PasienService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/pasien")
@CrossOrigin("http://localhost:3000")
public class PasienControler {

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    PasienService pasienService;

    @GetMapping("/{id}") // melihat data sesuai id
    public CommoneResponse<Pasien> getId (@PathVariable("id") Long id) {
        return ResponseHelper.ok(pasienService.getId(id));
    }

    @GetMapping("/all-userId")// untuk menampilkan semua data
    public CommoneResponse<Page<Pasien>> allSiswaId(@RequestParam(name = "userId") Long userId, @RequestParam(name = "page") Long page) {
        return ResponseHelper.ok(pasienService.allSiswaId(page, userId));
    }

    @GetMapping("/getAll-guru")// untuk menampilkan semua data
    public CommoneResponse<List<Pasien>> allGuru(@RequestParam(name = "userId") Long userId) {
        return ResponseHelper.ok(pasienService.allGuru(userId));
    }

    @GetMapping("/all-siswa")// untuk menampilkan semua data
    public CommoneResponse<List<Pasien>> allSiswa(@RequestParam(name = "userId") Long userId) {
        return ResponseHelper.ok(pasienService.allSiswa( userId));
    }

    @GetMapping("/all-karyawan")// untuk menampilkan semua data
    public CommoneResponse<List<Pasien>> allKaryawan(@RequestParam(name = "userId") Long userId) {
        return ResponseHelper.ok(pasienService.allKaryawan(userId));
    }

    @PostMapping("post-guru") //untuk menambahkan data
    public CommoneResponse<Pasien> addPasienGuru (PasienDto pasienDto) {
        return ResponseHelper.ok(pasienService.addPasien(modelMapper.map(pasienDto, PasienDto.class)));
    }

    @PostMapping("post-siswa") //untuk menambahkan data
    public CommoneResponse<Pasien> addPasienSiswa (PasienSiswaDto pasienSiswaDto) {
        return ResponseHelper.ok(pasienService.addSiswa(modelMapper.map(pasienSiswaDto, PasienSiswaDto.class)));
    }

    @PostMapping("post-karyawan") //untuk menambahkan data
    public CommoneResponse<Pasien> addPasienKaryawan (PasienDto pasienKaryawanDto) {
        return ResponseHelper.ok(pasienService.addKaryawan(modelMapper.map(pasienKaryawanDto, PasienDto.class)));
    }

    @PutMapping("/{id}_guru") //untuk mengedit data per id
    public CommoneResponse<Pasien> editGuru (@PathVariable("id") Long id, @RequestBody PasienDto pasienGuruDto) {
        return ResponseHelper.ok(pasienService.editPasien(id, pasienGuruDto));
    }

    @PutMapping("/{id}_siswa") //untuk mengedit data per id
    public CommoneResponse<Pasien> editSiswa (@PathVariable("id") Long id, @RequestBody PasienSiswaDto pasienSiswaDto) {
        return ResponseHelper.ok(pasienService.editSiswa(id, pasienSiswaDto));
    }

    @PutMapping("/{id}_karyawan") //untuk mengedit data per id
    public CommoneResponse<Pasien> editKaryawan(@PathVariable("id") Long id, @RequestBody PasienDto pasienKaryawanDto) {
        return ResponseHelper.ok(pasienService.editKaryawan(id, pasienKaryawanDto));
    }

    @DeleteMapping("/{id}")// untuk deleete per id
    public CommoneResponse<?> deleteById (@PathVariable("id") Long id) {
        return ResponseHelper.ok(pasienService.deleteById(id));
    }
}
