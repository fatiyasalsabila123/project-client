package com.be.projectserver.controler;

import com.be.projectserver.dto.ExcelDto;
import com.be.projectserver.impl.ExcelDaftarKaryawanImpl;
import com.be.projectserver.model.ExcelDaftarKaryawan;
import com.be.projectserver.model.ResponseMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/excel_daftar_karyawan")
public class ExcelDaftarKaryawanControler {


    @Autowired
    ExcelDaftarKaryawanImpl excelDaftarKaryawan;

    @GetMapping("/download/templatedaftarkaryawan")
    public ResponseEntity<Resource> getFileTemplate() {
        String filename = "template_daftar_karyawan.xlsx";
        InputStreamResource file = new InputStreamResource(excelDaftarKaryawan.pExcell());

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body(file);
    }

    @GetMapping("/download")
    public ResponseEntity<Resource> getFileKaryawan(ExcelDto excelDto, Long userId) {
        String filename = "daftar_karyawan.xlsx";
        InputStreamResource file = new InputStreamResource(excelDaftarKaryawan.loadKaryawan(excelDto, userId));

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body(file);
    }

    @PostMapping(path = "/upload/daftar_karyawan")
    public ResponseEntity<ResponseMessage> uploadFileKaryawan(@RequestPart("file") MultipartFile file, ExcelDto excelDto) {
        String message = "";
        if (ExcelDaftarKaryawan.hasExcelFormat(file)) {
            try {
                excelDaftarKaryawan.saveDaftarKaryawan(file, excelDto);
                message = "Uploaded the file successfully: " + file.getOriginalFilename();
                return ResponseEntity.status(HttpStatus.OK).body (new ResponseMessage(message));

            } catch (Exception e) {
                System.out.println(e);
                message = "Could not upload the file: " + file.getOriginalFilename() + "!";
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
            }
        }
        message = "Please upload an excel file!";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage(message));
    }
}
