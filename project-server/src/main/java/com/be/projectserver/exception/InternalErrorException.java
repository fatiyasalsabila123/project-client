package com.be.projectserver.exception;

public class InternalErrorException extends RuntimeException{

    public InternalErrorException(String message) {
        super(message);
    }
}
