package com.be.projectserver.exception;

public class EmailCondition extends RuntimeException{

    public EmailCondition(String message) {
        super(message);
    }
}
