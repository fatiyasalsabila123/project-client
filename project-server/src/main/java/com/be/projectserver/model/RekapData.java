package com.be.projectserver.model;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

public class RekapData {

    static String[] HEADERS = {"nama pasien", "status pasien", "tindakan", "tanggal", "status", "catatan" };

    static String SHEET = "Sheet1";

    public static ByteArrayInputStream rekapDataToExcel(List<PeriksaPasien> periksaPasiens) {
        try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) {
            Sheet sheet = workbook.createSheet(SHEET);

            Row headerRow = sheet.createRow(0);

            for (int col = 0; col < HEADERS.length; col++) {
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(HEADERS[col]);
            }

            int rowIdx = 1;
            for (PeriksaPasien periksaPasien : periksaPasiens) {
                Row row = sheet.createRow(rowIdx++);
                row.createCell(1).setCellValue(periksaPasien.getNamaPasien().getUsername());
                row.createCell(0).setCellValue(periksaPasien.getStatus());
                row.createCell(2).setCellValue(periksaPasien.getKeluhan());
                row.createCell(3).setCellValue(periksaPasien.getTanggal());
                row.createCell(4).setCellValue(periksaPasien.getPenyakitId().getNamaDiagnosa());
                row.createCell(5).setCellValue(periksaPasien.getPenangananId().getNamaPenanganan());
                row.createCell(6).setCellValue(periksaPasien.getTindakanId().getNamaTindakan());
            }

            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        } catch (IOException e) {
            throw new RuntimeException("fail to import data to Excel file: " + e.getMessage());
        }
    }
}
