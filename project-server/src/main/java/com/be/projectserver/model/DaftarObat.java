package com.be.projectserver.model;

import com.be.projectserver.user.model.Register;

import javax.persistence.*;

@Entity
@Table(name = "daftar_obat")
public class DaftarObat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nama_obat")
    private String namaObat;

    @Column(name = "stock")
    private long stock;

    @Column(name = "tanggal_expired")
    private String tanggalExpired;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private Register userId;

    public DaftarObat() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNamaObat() {
        return namaObat;
    }

    public void setNamaObat(String namaObat) {
        this.namaObat = namaObat;
    }

    public long getStock() {
        return stock;
    }

    public void setStock(long stock) {
        this.stock = stock;
    }

    public String getTanggalExpired() {
        return tanggalExpired;
    }

    public void setTanggalExpired(String tanggalExpired) {
        this.tanggalExpired = tanggalExpired;
    }

    public Register getUserId() {
        return userId;
    }

    public void setUserId(Register userId) {
        this.userId = userId;
    }
}
