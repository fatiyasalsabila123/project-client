package com.be.projectserver.model;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

public class ExcelSudahDitangani {

        static String[] HEADERS = {"nama pasien", "status pasien", "tindakan", "tanggal", "status", "catatan" };

    static String SHEET = "Sheet1";

    public static ByteArrayInputStream sudahDitanganiToExcel(PeriksaPasien periksaPasiens) {
        try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) {
            Sheet sheet = workbook.createSheet(SHEET);

            Row headerRow = sheet.createRow(0);

            for (int col = 0; col < HEADERS.length; col++) {
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(HEADERS[col]);
            }

            int rowIdx = 1;


//            for (PeriksaPasien periksaPasien : periksaPasiens) {
                Row row = sheet.createRow(rowIdx++);
                row.createCell(0).setCellValue(periksaPasiens.getNamaPasien().getUsername());
                row.createCell(1).setCellValue(periksaPasiens.getStatusPasien());
                row.createCell(2).setCellValue(periksaPasiens.getTindakanId().getNamaTindakan());
                row.createCell(3).setCellValue(periksaPasiens.getTanggal());
                row.createCell(4).setCellValue(periksaPasiens.getStatus());
                row.createCell(5).setCellValue(periksaPasiens.getKeluhan());
//            }

            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        }catch (IOException e) {
            throw new RuntimeException("fail to import data to excel file " + e.getMessage());
        }
    }

}
