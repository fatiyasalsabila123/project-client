package com.be.projectserver.model;

import com.be.projectserver.user.model.Register;

import javax.persistence.*;

@Entity
@Table(name = "penanganan")
public class Penanganan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nama_penanganan")
    private String namaPenanganan;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private Register userId;

    public Penanganan() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNamaPenanganan() {
        return namaPenanganan;
    }

    public void setNamaPenanganan(String namaPenanganan) {
        this.namaPenanganan = namaPenanganan;
    }

    public Register getUserId() {
        return userId;
    }

    public void setUserId(Register userId) {
        this.userId = userId;
    }
}
