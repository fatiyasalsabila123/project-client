package com.be.projectserver.model;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class ExcelDaftarGuru {

    public static String TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    static String[] HEADERS = {"nama_guru", "tempat", "tanggal_lahir", "alamat" };

    static String SHEET = "Sheet1";

    public static boolean hasExcelFormat(MultipartFile file) {
        if (!TYPE.equals(file.getContentType())) {
            return false;
        }
        return true;
    }

    public static ByteArrayInputStream daftarGuruToExcel(List<Pasien> pasiens) {
        try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) {
            Sheet sheet = workbook.createSheet(SHEET);

            Row headerRow = sheet.createRow(0);

            for (int col = 0; col < HEADERS.length; col++) {
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(HEADERS[col]);
            }

            int rowIdx = 1;
            for (Pasien pasien : pasiens) {
                Row row = sheet.createRow(rowIdx++);
                row.createCell(0).setCellValue(pasien.getUsername());
                row.createCell(1).setCellValue(pasien.getTempat());
                row.createCell(2).setCellValue(pasien.getTanggalLahir());
                row.createCell(3).setCellValue(pasien.getAlamat());
            }
            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        } catch (IOException e) {
            throw new RuntimeException("fail to import data to excel file: " + e.getMessage());
        }
    }

    private static final int Hour = 1000 * 3600;
    public static List<Pasien> excelToDaftarGuru(InputStream is) {
        try {
            Workbook workbook = new XSSFWorkbook(is);

            Sheet sheet = workbook.getSheet(SHEET);
            Iterator<Row> rows = sheet.iterator();

            List<Pasien> pasienList = new ArrayList<Pasien>();

            int rowNumber = 0;
            while (rows.hasNext()) {
                Row currentRow = rows.next();

                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }

                Iterator<Cell> cellsInRow = currentRow.iterator();

                Pasien pasien = new Pasien();

                int cellIdx = 0;
                while (cellsInRow.hasNext()) {
                    Cell currentCell = cellsInRow.next();

                    switch (cellIdx) {
                        case 0:
                            pasien.setUsername(currentCell.getStringCellValue());
                            break;
                        case 1:
                            pasien.setTempat(currentCell.getStringCellValue());
                            break;
                        case 2:
                            pasien.setTanggalLahir(currentCell.getStringCellValue());
                            break;
                        case 3 :
                            pasien.setAlamat(currentCell.getStringCellValue());
                            break;

                        default:
                            break;
                    }
                    pasienList.add(pasien);
                    cellIdx++;
                }
                pasien.setWaktu((new Date(new Date().getTime() + 24 * 5 * Hour)));
                pasien.setJabatan("Guru");
                pasien.setStatus("guru");
            }
            workbook.close();
            return pasienList;
        } catch (IOException e) {
            throw new RuntimeException("fail to parse excel file: " + e.getMessage());
        }
    }
}
