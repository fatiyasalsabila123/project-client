package com.be.projectserver.dto;

public class DaftarObatDto {

    private String namaObat;

    private long stok;

    private String tanggalExpired;

    private Long userId;

    public String getNamaObat() {
        return namaObat;
    }

    public void setNamaObat(String namaObat) {
        this.namaObat = namaObat;
    }

    public long getStok() {
        return stok;
    }

    public void setStok(long stok) {
        this.stok = stok;
    }

    public String getTanggalExpired() {
        return tanggalExpired;
    }

    public void setTanggalExpired(String tanggalExpired) {
        this.tanggalExpired = tanggalExpired;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
