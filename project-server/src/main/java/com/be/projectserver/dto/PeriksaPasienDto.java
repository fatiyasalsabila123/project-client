package com.be.projectserver.dto;

import java.util.Date;

public class PeriksaPasienDto {

    private Long namaPasien;

    private String StatusPasien;

    private Date tanggal;

    private Long userId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    public Long getNamaPasien() {
        return namaPasien;
    }

    public void setNamaPasien(Long namaPasien) {
        this.namaPasien = namaPasien;
    }

    public String getStatusPasien() {
        return StatusPasien;
    }

    public void setStatusPasien(String statusPasien) {
        StatusPasien = statusPasien;
    }

    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private String keluhan;

    public String getKeluhan() {
        return keluhan;
    }

    public void setKeluhan(String keluhan) {
        this.keluhan = keluhan;
    }
}
