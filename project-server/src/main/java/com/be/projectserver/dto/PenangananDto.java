package com.be.projectserver.dto;

public class PenangananDto {

    private String namaPenanganan;

    private Long userId;

    public String getNamaPenanganan() {
        return namaPenanganan;
    }

    public void setNamaPenanganan(String namaPenanganan) {
        this.namaPenanganan = namaPenanganan;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
