package com.be.projectserver.user.impl;

import com.be.projectserver.exception.InternalErrorException;
import com.be.projectserver.exception.NotFoundException;
import com.be.projectserver.user.Dto.RegisterDto;
import com.be.projectserver.user.jwt.JwtProvider;
import com.be.projectserver.user.model.AboutDto;
import com.be.projectserver.user.model.Login;
import com.be.projectserver.user.model.Register;
import com.be.projectserver.user.model.TextDto;
import com.be.projectserver.user.repository.RegisterRepository;
import com.be.projectserver.user.roleEnum.RoleEnum;
import com.be.projectserver.user.service.RegisterService;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

@Service
public class RegisterImpl implements RegisterService {

    @Autowired
    private RegisterRepository registerRepository;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtProvider jwtProvider;

    @Autowired
    UserDetailsService userDetailsService;

    @Autowired
    PasswordEncoder passwordEncoder;

    private String authories(String email, String password) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
        } catch (BadCredentialsException e) {
            throw new InternalErrorException("email or password not found");
        }
        UserDetails userDetails = userDetailsService.loadUserByUsername(email);
        return jwtProvider.generateToken(userDetails);
    }
    @Override
    public Register register(Register register) {
        String UserPassword = register.getPassword().trim();
        boolean PasswordIsNotValid = !UserPassword.matches("^(?=.*[0-8])(?=.*[a-z])(?=\\S+$).{8,20}");
        if (PasswordIsNotValid) throw new InternalErrorException("password not valid");
        boolean isEmail = Pattern.compile("^(.+)@(\\S+)$")
                .matcher(register.getEmail()).matches();
        if (!isEmail) throw new InternalErrorException("Email Not Valid");
        register.setRole(RoleEnum.ADMIN);
        register.setPassword(passwordEncoder.encode(register.getPassword()));
        return registerRepository.save(register);
    }

    @Override
    public Map<String, Object> login(Login login) {
        String token = authories(login.getEmail(), login.getPassword());
        Register register ;

//        mengecek email
        boolean isEmail = Pattern.compile("^(.+)@(\\S+)$")
                .matcher(login.getEmail()).matches();
        System.out.println("is Email " + isEmail);

//        jika true, akan menjalankan sistem if
        if(isEmail) {
            register = registerRepository.findByEmail(login.getEmail());
        } else { // jika false, else akan dijalankan, dgn login username
            register = registerRepository.findByUsername(login.getEmail());
        }

        Map<String, Object> response = new HashMap<>();
        response.put("token", token);
        response.put("expired", "15 menit");
        response.put("user", register);
        return response;
    }

    @Override
    public Register getById(Long id) {
        return registerRepository.findById(id).orElseThrow(() -> new NotFoundException("id not found"));
    }

    @Override
    public Register update(Long id, Register register) {
        Register register1 = registerRepository.findById(id).orElseThrow(() -> new NotFoundException("id not found"));
        register1.setEmail(register.getEmail());
        register1.setAlamat(register.getAlamat());
        register1.setTanggalLahir(register.getTanggalLahir());
        boolean isEmail = Pattern.compile("^(.+)@(\\S+)$")
                .matcher(register.getEmail()).matches();
        if (!isEmail)throw new InternalErrorException("Email Not Valid");
//        }
        register1.setUsername(register.getUsername());
        return registerRepository.save(register1);
    }

    @Override
    public Register updatePAssword(Long id, Register register) {
        Register register1 = registerRepository.findById(id).orElseThrow(() -> new NotFoundException("id not found"));
        String UserPassword = register.getPassword().trim();
        boolean PasswordIsNOtValid = !UserPassword.matches("^(?=.*[0-9])(?=.*[a-z])(?=\\S+$).{8,20}");
        if (PasswordIsNOtValid) throw new InternalErrorException("password not valid");
        register1.setPassword(passwordEncoder.encode(register.getPassword()));
        return registerRepository.save(register1);
    }

    @Override
    public Register updateFoto(Long id, Register register, MultipartFile multipartFile) {
        Register update = registerRepository.findById(id).orElseThrow(() -> new NotFoundException("Id Not found"));
        String url = convertToBase64(multipartFile);
        update.setFoto(url);
        return registerRepository.save(update);
    }

    @Override
    public Register updateAbout(Long id, AboutDto aboutDto) {
        Register aboutDto1 = registerRepository.findById(id).orElseThrow(() -> new NotFoundException("Id Not found"));
        aboutDto1.setAbout(aboutDto.getAbout());
        return registerRepository.save(aboutDto1);
    }

    @Override
    public Register updateText(Long id, TextDto textDto) {
        Register text = registerRepository.findById(id).orElseThrow(() -> new NotFoundException("Id Not found"));
        text.setText(text.getText());
        return registerRepository.save(text);
    }

    @Override
    public Map<String, Boolean> deleteSekolah(Long id) {
        try {
            registerRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }

    @Override
    public Page<Register> getAll(String query, Long page) {
        Pageable pageable = PageRequest.of(Math.toIntExact(page), 5);
        return registerRepository.findAll(query, pageable);
    }

    private String convertToBase64(MultipartFile file) {
        String url = "";
        try {
            byte[] byteData = Base64.encodeBase64(file.getBytes());
            String res = new String(byteData);
            url = "data:" + file.getContentType() + ";base64," + res;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return url;
        }
    }
}
