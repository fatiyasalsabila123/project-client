package com.be.projectserver.user.service;

import com.be.projectserver.user.Dto.RegisterDto;
import com.be.projectserver.user.model.AboutDto;
import com.be.projectserver.user.model.Login;
import com.be.projectserver.user.model.Register;
import com.be.projectserver.user.model.TextDto;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

public interface RegisterService {

    Register register(Register register);

    Map<String, Object> login (Login login);

    Register getById(Long id);

    Register update(Long id, Register register);

    Register updatePAssword(Long id, Register register);

    Register updateFoto (Long id, Register register, MultipartFile multipartFile);

    Register updateAbout(Long id, AboutDto aboutDto);

    Register updateText(Long id, TextDto textDto);

    Map<String, Boolean> deleteSekolah(Long id);

    Page<Register> getAll(String query, Long page);

}
