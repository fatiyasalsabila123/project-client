package com.be.projectserver.user.impl;

import com.be.projectserver.user.model.Register;
import com.be.projectserver.user.model.UserPrinciple;
import com.be.projectserver.user.repository.RegisterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.regex.Pattern;

@Service
public class UserDetailsImpl implements UserDetailsService {

    @Autowired
    private RegisterRepository registerRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //        mengecek email
        boolean isEmail = Pattern.compile("^(.+)@(\\S+)$")
                .matcher(username).matches();
        Register user;
        System.out.println("is Email " + isEmail);

//        jika email ada
        if(isEmail) {
            user = registerRepository.findByEmail(username);
        } else { // else username
            user = registerRepository.findByUsername(username);
        }
        return UserPrinciple.build(user);
    }
}
