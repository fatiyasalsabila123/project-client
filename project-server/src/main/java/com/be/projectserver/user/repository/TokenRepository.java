package com.be.projectserver.user.repository;

import com.be.projectserver.user.model.TemporaryToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TokenRepository extends JpaRepository<TemporaryToken, Long> {

    Optional<TemporaryToken> findByToken(String token);
    Optional<TemporaryToken> findByRegisterId(long registerId);

}
