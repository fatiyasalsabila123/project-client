package com.be.projectserver.repository;

import com.be.projectserver.model.DaftarObat;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DaftarObatRepository extends JpaRepository<DaftarObat, Long> {

    @Query(value = "SELECT * FROM daftar_obat WHERE nama_obat LIKE CONCAT('%', :query, '%')", nativeQuery = true)
    Page<DaftarObat> searchfinAll(String query, Pageable pageable);

    @Query(value = "SELECT * FROM daftar_obat WHERE user_id = ?1", nativeQuery = true)
    Page<DaftarObat> getAllDaftarObat(Pageable pageable, Long userId);


}
