package com.be.projectserver.repository;

import com.be.projectserver.model.DaftarObat;
import com.be.projectserver.model.Tindakan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TindakanRepository extends JpaRepository<Tindakan, Long> {

    @Query(value = "SELECT * FROM tindakan WHERE nama_tindakan LIKE CONCAT ('%', :query, '%')", nativeQuery = true)
    Page<Tindakan> searchfinAll(String query, Pageable pageable);

    @Query(value = "SELECT * FROM tindakan WHERE user_id = ?1", nativeQuery = true)
    Page<Tindakan> getAllTindakan(Pageable pageable, Long userId);

}
