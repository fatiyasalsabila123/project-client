package com.be.projectserver.repository;

import com.be.projectserver.model.DaftarObat;
import com.be.projectserver.model.Diagnosa;
import com.be.projectserver.model.Penanganan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PenangananRepository extends JpaRepository<Penanganan, Long> {

    @Query(value = "SELECT * FROM penanganan WHERE nama_penanganan LIKE CONCAT('%', :query, '%')", nativeQuery = true)
    Page<Penanganan> searchfindAll (String query, Pageable pageable);

    @Query(value = "SELECT * FROM penanganan WHERE user_id = ?1", nativeQuery = true)
    Page<Penanganan> getAllPenanganan(Pageable pageable, Long userId);

}
