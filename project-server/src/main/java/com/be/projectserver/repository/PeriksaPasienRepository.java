package com.be.projectserver.repository;

import com.be.projectserver.impl.PeriksaPasienImpl;
import com.be.projectserver.model.Pasien;
import com.be.projectserver.model.PeriksaPasien;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Repository
public interface PeriksaPasienRepository extends JpaRepository<PeriksaPasien, Long> {

    @Query(value = "SELECT * FROM periksa_pasien WHERE user_id = ?1 ", nativeQuery = true)
    Page<PeriksaPasien> allPasien (Long userId, Pageable pageable );


    @Query(value = "SELECT * FROM periksa_pasien WHERE tanggal >= :starttanggal AND tanggal <= :endtanggal", nativeQuery = true)
    List<PeriksaPasien> filterTanggal(String starttanggal, String endtanggal);

}
