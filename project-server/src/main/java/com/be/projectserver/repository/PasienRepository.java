package com.be.projectserver.repository;

import com.be.projectserver.model.Pasien;
import com.be.projectserver.model.PeriksaPasien;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public interface PasienRepository extends JpaRepository<Pasien, Long> {

    @Query(value = "SELECT * FROM pasien WHERE siswa_id = ?1", nativeQuery = true)
    Page<Pasien> getAllUserId(Pageable pageable, Long userId);

    @Query(value = "SELECT * FROM pasien WHERE status LIKE CONCAT ('guru') AND siswa_id = ?1", nativeQuery = true)
    List<Pasien> getAllGuru( Long userId);

    @Query(value = "SELECT * FROM pasien WHERE status LIKE CONCAT ('siswa') AND siswa_id = ?1", nativeQuery = true)
    List<Pasien> getAllSiswa(Long userId);

    @Query(value = "SELECT * FROM pasien WHERE status LIKE CONCAT ('karyawan') AND siswa_id = ?1", nativeQuery = true)
    List<Pasien> getAllKaryawan(Long userId);


}
