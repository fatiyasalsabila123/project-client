package com.be.projectserver.repository;

import com.be.projectserver.model.DaftarObat;
import com.be.projectserver.model.Diagnosa;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface DiagnosaRepository extends JpaRepository<Diagnosa, Long> {

    @Query(value = "SELECT * FROM diagnosa WHERE nama_diagnosa LIKE CONCAT('%', :query, '%')", nativeQuery = true)
    Page<Diagnosa> searchfindAll (String query, Pageable pageable);

    @Query(value = "SELECT * FROM diagnosa WHERE user_id = ?1", nativeQuery = true)
    Page<Diagnosa> getAllDiangnosa(Pageable pageable, Long userId);

}
