package com.be.projectserver.impl;

import com.be.projectserver.dto.ExcelDto;
import com.be.projectserver.dto.PeriksaPasienDto;
import com.be.projectserver.dto.TanganiDto;
import com.be.projectserver.exception.NotFoundException;
import com.be.projectserver.model.Pasien;
import com.be.projectserver.model.PeriksaPasien;
import com.be.projectserver.repository.*;
import com.be.projectserver.service.PeriksaPasienService;
import com.be.projectserver.user.model.Register;
import com.be.projectserver.user.service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Service
public class PeriksaPasienImpl implements PeriksaPasienService {

    @Autowired
    PeriksaPasienRepository periksaPasienRepository;

    @Autowired
    DiagnosaRepository diagnosaRepository;

    @Autowired
    PenangananRepository penangananRepository;

    @Autowired
    TindakanRepository tindakanRepository;

    @Autowired
    PasienRepository pasienRepository;

    @Autowired
    RegisterService registerService;


    @Override
    public Page<PeriksaPasien> allPasien(Long userId, Long page) {
        Pageable pageable = PageRequest.of(Math.toIntExact(page), 10);
        return periksaPasienRepository.allPasien(userId, pageable);
    }

    private static final int Hour = 1000 * 3600;
    @Override
    public PeriksaPasien addPasien(PeriksaPasienDto periksaPasien) {
        PeriksaPasien periksaPasien1 = new PeriksaPasien();
        Register register = registerService.getById(periksaPasien.getUserId());
        periksaPasien1.setUserId(register);
        periksaPasien1.setKeluhan(periksaPasien.getKeluhan());
        periksaPasien1.setStatusPasien(periksaPasien.getStatusPasien());
        periksaPasien1.setTanggal(periksaPasien.getTanggal());
        periksaPasien1.setStatus("belum ditangani");
        periksaPasien1.setWaktu((new Date(new Date().getTime() + 24 * 5 * Hour)));
        periksaPasien1.setNamaPasien(pasienRepository.findById(periksaPasien.getNamaPasien()).orElseThrow(() -> new NotFoundException("id not found")));
        return periksaPasienRepository.save(periksaPasien1);
    }

    @Override
    public PeriksaPasien editTangani(Long id, TanganiDto tanganiDto) {
        PeriksaPasien periksaPasien = periksaPasienRepository.findById(id).orElseThrow(() -> new NotFoundException("Not Found"));
        periksaPasien.setStatus("sudah ditangani");
        periksaPasien.setPenyakitId(diagnosaRepository.findById(tanganiDto.getPanyakitId()).orElseThrow(() -> new NotFoundException("id not found")));
        periksaPasien.setPenangananId(penangananRepository.findById(tanganiDto.getPenangananId()).orElseThrow(() -> new NotFoundException("id not found")));
        periksaPasien.setTindakanId(tindakanRepository.findById(tanganiDto.getTindakanId()).orElseThrow(() -> new NotFoundException("id not found")));
        periksaPasien.setKeluhan(tanganiDto.getKeluhan());
        return periksaPasienRepository.save(periksaPasien);
    }

    @Override
    public PeriksaPasien getId(Long id) {
        var periksaPasien = periksaPasienRepository.findById(id).get();
        return periksaPasienRepository.save(periksaPasien);
    }

    @Override
    public List<PeriksaPasien> filterTanggal(String starttanggal, String endtanggal) {
        return periksaPasienRepository.filterTanggal(starttanggal, endtanggal);
    }

}
