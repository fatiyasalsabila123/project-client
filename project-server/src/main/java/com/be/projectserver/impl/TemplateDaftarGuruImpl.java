package com.be.projectserver.impl;

import com.be.projectserver.model.Pasien;
import com.be.projectserver.model.TemplateDaftarGuru;
import com.be.projectserver.repository.PasienRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.util.List;

@Service
public class TemplateDaftarGuruImpl {

    @Autowired
    PasienRepository pasienRepository;

    public ByteArrayInputStream pExcell() {
        List<Pasien> pendaftaranSiswas = pasienRepository.findAll();
        ByteArrayInputStream in = TemplateDaftarGuru.templateToExcel(pendaftaranSiswas);
        return in;
    }
}
