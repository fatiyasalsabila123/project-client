package com.be.projectserver.impl;

import com.be.projectserver.dto.ExcelDto;
import com.be.projectserver.model.ExcelDaftarGuru;
import com.be.projectserver.model.Pasien;
import com.be.projectserver.repository.PasienRepository;
import com.be.projectserver.user.service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

@Service
public class ExcelDaftarGuruImpl {

    @Autowired
    PasienRepository pasienRepository;

    @Autowired
    RegisterService registerService;

    public ByteArrayInputStream loadGuru(ExcelDto excelDto, Long userId) {
        List<Pasien> pasiens = pasienRepository.getAllGuru(userId);
        ByteArrayInputStream in = ExcelDaftarGuru.daftarGuruToExcel(pasiens);
        return in;
    }

    public void saveDaftarGuru(MultipartFile file, ExcelDto excelDto) {
        try {
            List<Pasien> pasienList = ExcelDaftarGuru.excelToDaftarGuru(file.getInputStream());
            for (int i = 0 ; i < pasienList.size() -1 ; i ++ ) {
                pasienList.get(i).setSiswaId(registerService.getById(excelDto.getUserId()));
            }
            pasienRepository.saveAll(pasienList);
        } catch (IOException e) {
            throw new RuntimeException("fail to store excel data: " + e.getMessage());
        }
    }
}
