package com.be.projectserver.impl;

import com.be.projectserver.dto.TindakanDto;
import com.be.projectserver.exception.NotFoundException;
import com.be.projectserver.model.DaftarObat;
import com.be.projectserver.model.Tindakan;
import com.be.projectserver.repository.TindakanRepository;
import com.be.projectserver.service.TindakanService;
import com.be.projectserver.user.model.Register;
import com.be.projectserver.user.service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TindakanImpl implements TindakanService {

    @Autowired
    TindakanRepository tindakanRepository;

    @Autowired
    RegisterService registerService;

    @Override
    public Page<Tindakan> getAll(String query, Long page) {
        Pageable pageable = PageRequest.of(Math.toIntExact(page), 10);
        return tindakanRepository.searchfinAll(query, pageable);
    }

    @Override
    public Tindakan addTindakan(TindakanDto tindakan) {
        Tindakan tindakan1 = new Tindakan();
        Register register= registerService.getById(tindakan.getUserId());
        tindakan1.setNamaTindakan(tindakan.getNamaTindakan());
        tindakan1.setUserId(register);
        return tindakanRepository.save(tindakan1);
    }

    @Override
    public Tindakan editTindakan(Long id, Tindakan tindakan) {
        Tindakan tindakan1 = tindakanRepository.findById(id).orElseThrow(() -> new NotFoundException("id not found"));
        tindakan1.setNamaTindakan(tindakan.getNamaTindakan());
        return tindakanRepository.save(tindakan1);
    }

    @Override
    public Page<Tindakan> allTindakan(Long page, Long userId) {
        Pageable pageable = PageRequest.of(Math.toIntExact(page), 5);
        return tindakanRepository.getAllTindakan(pageable, userId);
    }

    @Override
    public Map<String, Boolean> deleteById(Long id) {
        try {
            tindakanRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("Hapus", Boolean.TRUE);
            return res;
        } catch (Exception e) {

            Map<String, Boolean> tes = new HashMap<>();
            tes.put("Hapus", Boolean.FALSE);
            return tes;
        }
    }

    @Override
    public Tindakan getIdTindakan(Long id) {
        var tindakan = tindakanRepository.findById(id).orElseThrow(() -> new NotFoundException("id not found"));
        return tindakanRepository.save(tindakan);
    }
}
