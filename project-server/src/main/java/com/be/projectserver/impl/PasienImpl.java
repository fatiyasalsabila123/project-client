package com.be.projectserver.impl;

import com.be.projectserver.dto.PasienDto;
import com.be.projectserver.dto.PasienGuruDto;
import com.be.projectserver.dto.PasienKaryawanDto;
import com.be.projectserver.dto.PasienSiswaDto;
import com.be.projectserver.exception.NotFoundException;
import com.be.projectserver.model.Pasien;
import com.be.projectserver.repository.PasienRepository;
import com.be.projectserver.service.PasienService;
import com.be.projectserver.user.model.Register;
import com.be.projectserver.user.service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PasienImpl implements PasienService {

    @Autowired
    PasienRepository pasienRepository;

    @Autowired
    RegisterService registerService;


    @Override
    public List<Pasien> allGuru(Long userId) {
        return pasienRepository.getAllGuru(userId);
    }

    @Override
    public List<Pasien> allSiswa(Long userId) {
        return pasienRepository.getAllSiswa(userId);
    }

    @Override
    public List<Pasien> allKaryawan(Long userId) {
        return pasienRepository.getAllKaryawan(userId);
    }

    @Override
    public Page<Pasien> allSiswaId(Long page, Long userId) {
        Pageable pageable = PageRequest.of(Math.toIntExact(page),10);
        return pasienRepository.getAllUserId(pageable, userId);
    }

    private static final int Hour = 1000 * 3600;

    @Override
    public Pasien addPasien(PasienDto pasienDto) {
        Pasien pasien = new Pasien();
        Register register = registerService.getById(pasienDto.getUserId());
        pasien.setSiswaId(register);
        pasien.setUsername(pasienDto.getUsername());
        pasien.setJabatan(pasienDto.getJabatan());
        pasien.setStatus("guru");
        pasien.setTempat(pasienDto.getTempat());
        pasien.setTanggalLahir(pasienDto.getTanggalLahir());
        pasien.setTanggalPeriksa(pasienDto.getTanggalPeriksa());
        pasien.setAlamat(pasienDto.getAlamat());
        pasien.setWaktu((new Date(new Date().getTime() + 24 * 5 * Hour)));
        return pasienRepository.save(pasien);
    }

    @Override
    public Pasien addSiswa(PasienSiswaDto pasienDto) {
        Pasien pasien = new Pasien();
        Register register = registerService.getById(pasienDto.getUserId());
        pasien.setSiswaId(register);
        pasien.setUsername(pasienDto.getUsername());
        pasien.setJabatan(pasienDto.getJabatan());
        pasien.setStatus("siswa");
        pasien.setTempat(pasienDto.getTempat());
        pasien.setTanggalLahir(pasienDto.getTanggalLahir());
        pasien.setAlamat(pasienDto.getAlamat());
        pasien.setKelas(pasienDto.getKelas());
        pasien.setTanggalPeriksa(pasienDto.getTanggalPeriksa());
        pasien.setWaktu((new Date(new Date().getTime() + 24 * 5 * Hour)));
        return pasienRepository.save(pasien);
    }

    @Override
    public Pasien addKaryawan(PasienDto pasienDto) {
        Pasien pasien = new Pasien();
        Register register = registerService.getById(pasienDto.getUserId());
        pasien.setSiswaId(register);
        pasien.setUsername(pasienDto.getUsername());
        pasien.setJabatan(pasienDto.getJabatan());
        pasien.setStatus("karyawan");
        pasien.setTempat(pasienDto.getTempat());
        pasien.setTanggalLahir(pasienDto.getTanggalLahir());
        pasien.setAlamat(pasienDto.getAlamat());
        pasien.setTanggalPeriksa(pasienDto.getTanggalPeriksa());
        pasien.setWaktu((new Date(new Date().getTime() + 24 * 5 * Hour)));
        return pasienRepository.save(pasien);
    }

    @Override
    public Pasien editPasien(Long id, PasienDto pasienDto) {
        Pasien pasien = pasienRepository.findById(id).orElseThrow(() -> new NotFoundException("Not Found"));
        pasien.setUsername(pasienDto.getUsername());
        pasien.setTanggalLahir(pasienDto.getTanggalLahir());
        pasien.setJabatan(pasienDto.getJabatan());
        pasien.setTempat(pasienDto.getTempat());
        pasien.setAlamat(pasienDto.getAlamat());
        return pasienRepository.save(pasien);
    }

    @Override
    public Pasien editSiswa(Long id, PasienSiswaDto pasienDto) {
        Pasien pasien = pasienRepository.findById(id).orElseThrow(() -> new NotFoundException("Not Found"));
        pasien.setUsername(pasienDto.getUsername());
        pasien.setTanggalLahir(pasienDto.getTanggalLahir());
        pasien.setJabatan(pasienDto.getJabatan());
        pasien.setTempat(pasienDto.getTempat());
        pasien.setAlamat(pasienDto.getAlamat());
        pasien.setKelas(pasienDto.getKelas());
        return pasienRepository.save(pasien);
    }

    @Override
    public Pasien editKaryawan(Long id, PasienDto pasienDto) {
        Pasien pasien = pasienRepository.findById(id).orElseThrow(() -> new NotFoundException("Not Found"));
        pasien.setUsername(pasienDto.getUsername());
        pasien.setTanggalLahir(pasienDto.getTanggalLahir());
        pasien.setJabatan(pasienDto.getJabatan());
        pasien.setTempat(pasienDto.getTempat());
        pasien.setAlamat(pasienDto.getAlamat());
        return pasienRepository.save(pasien);
    }


    @Override
    public Pasien getId(Long id) {
        var karyawan = pasienRepository.findById(id).orElseThrow(() -> new NotFoundException("id not found"));
        return pasienRepository.save(karyawan);
    }

    @Override
    public Map<String, Boolean> deleteById(Long id) {
        try {
            pasienRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("Hapus", Boolean.TRUE);
            return res;
        } catch (Exception e) {

            Map<String, Boolean> tes = new HashMap<>();
            tes.put("Hapus", Boolean.FALSE);
            return tes;
        }
    }
}
