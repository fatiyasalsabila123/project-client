package com.be.projectserver.impl;

import com.be.projectserver.dto.PenangananDto;
import com.be.projectserver.exception.NotFoundException;
import com.be.projectserver.model.DaftarObat;
import com.be.projectserver.model.Diagnosa;
import com.be.projectserver.model.Penanganan;
import com.be.projectserver.repository.PenangananRepository;
import com.be.projectserver.service.PenangananService;
import com.be.projectserver.user.model.Register;
import com.be.projectserver.user.service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PenangananImpl implements PenangananService {

    @Autowired
    PenangananRepository penangananRepository;

    @Autowired
    RegisterService registerService;

    @Override
    public Page<Penanganan> getAll(String query, Long page) {
        Pageable pageable = PageRequest.of(Math.toIntExact(page), 5);
        return penangananRepository.searchfindAll(query, pageable);
    }

    @Override
    public Penanganan addPenanganan(PenangananDto penanganan) {
        Penanganan penanganan1 = new Penanganan();
        Register register= registerService.getById(penanganan.getUserId());
        penanganan1.setNamaPenanganan(penanganan.getNamaPenanganan());
        penanganan1.setUserId(register);
        return penangananRepository.save(penanganan1);
    }

    @Override
    public Penanganan editPenanganan(Long id, Penanganan penanganan) {
        Penanganan penanganan1 = penangananRepository.findById(id).orElseThrow(() -> new NotFoundException("id not found"));
        penanganan1.setNamaPenanganan(penanganan.getNamaPenanganan());
        return penangananRepository.save(penanganan1);
    }

    @Override
    public Penanganan getId(Long id) {
        var penanganan = penangananRepository.findById(id).get();
        return penangananRepository.save(penanganan);
    }

    @Override
    public Map<String, Boolean> deleteById(Long id) {
        try {
            penangananRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("Hapus", Boolean.TRUE);
            return res;
        } catch (Exception e) {

            Map<String, Boolean> tes = new HashMap<>();
            tes.put("Hapus", Boolean.FALSE);
            return tes;
        }
    }

    @Override
    public Page<Penanganan> allPenanganan(Long page, Long userId) {
        Pageable pageable = PageRequest.of(Math.toIntExact(page), 10);
        return penangananRepository.getAllPenanganan(pageable, userId);
    }
}
