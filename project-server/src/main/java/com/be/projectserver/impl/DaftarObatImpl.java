package com.be.projectserver.impl;

import com.be.projectserver.dto.DaftarObatDto;
import com.be.projectserver.exception.NotFoundException;
import com.be.projectserver.model.DaftarObat;
import com.be.projectserver.repository.DaftarObatRepository;
import com.be.projectserver.service.DaftarObatService;
import com.be.projectserver.user.model.Register;
import com.be.projectserver.user.service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DaftarObatImpl implements DaftarObatService {

    @Autowired
    DaftarObatRepository daftarObatRepository;

    @Autowired
    RegisterService registerService;

    @Override
    public Page<DaftarObat> getAll(String query, Long page) {
        Pageable pageable = PageRequest.of(Math.toIntExact(page), 10);
        return daftarObatRepository.searchfinAll(query, pageable);
    }

    @Override
    public DaftarObat addDaftarObat(DaftarObatDto daftarObat) {
        DaftarObat daftarObat1 = new DaftarObat();
        Register register= registerService.getById(daftarObat.getUserId());
        daftarObat1.setNamaObat(daftarObat.getNamaObat());
        daftarObat1.setStock(daftarObat.getStok());
        daftarObat1.setTanggalExpired(daftarObat.getTanggalExpired());
        daftarObat1.setUserId(register);
        return daftarObatRepository.save(daftarObat1);
    }

    @Override
    public DaftarObat editDaftarObat(Long id, DaftarObat daftarObat) {
        DaftarObat daftarObat1 = daftarObatRepository.findById(id).orElseThrow(() -> new NotFoundException("id not found"));
        daftarObat1.setNamaObat(daftarObat.getNamaObat());
        daftarObat1.setStock(daftarObat.getStock());
        daftarObat1.setTanggalExpired(daftarObat.getTanggalExpired());
        return daftarObatRepository.save(daftarObat1);
    }

    @Override
    public DaftarObat getIdDaftarObat(Long id) {
        var daftarObar = daftarObatRepository.findById(id).orElseThrow(() -> new NotFoundException("id not found"));
        return daftarObatRepository.save(daftarObar);
    }

    @Override
    public Map<String, Boolean> deleteById(Long id) {
        try {
            daftarObatRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("Hapus", Boolean.TRUE);
            return res;
        } catch (Exception e) {

            Map<String, Boolean> tes = new HashMap<>();
            tes.put("Hapus", Boolean.FALSE);
            return tes;
        }
    }

    @Override
    public Page<DaftarObat> allDaftarObat( Long page, Long userId) {
        Pageable pageable = PageRequest.of(Math.toIntExact(page), 5);
        return daftarObatRepository.getAllDaftarObat(pageable, userId);
    }
}
