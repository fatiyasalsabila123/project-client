package com.be.projectserver.impl;

import com.be.projectserver.dto.DiagnosaDto;
import com.be.projectserver.model.DaftarObat;
import com.be.projectserver.model.Diagnosa;
import com.be.projectserver.repository.DiagnosaRepository;
import com.be.projectserver.service.DiagnosaService;
import com.be.projectserver.exception.NotFoundException;
import com.be.projectserver.user.model.Register;
import com.be.projectserver.user.service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DiagnosaIMpl implements DiagnosaService {

    @Autowired
    DiagnosaRepository diagnosaRepository;

    @Autowired
    RegisterService registerService;

    @Override
    public Page<Diagnosa> getAllDiagnosa(String query, Long page) {
        Pageable pageable = PageRequest.of(Math.toIntExact(page), 10 );
        return diagnosaRepository.searchfindAll(query, pageable);
    }

    @Override
    public Diagnosa addDiagnosa(DiagnosaDto diagnosa) {
        Diagnosa diagnosa1 = new Diagnosa();
        Register register= registerService.getById(diagnosa.getUserId());
        diagnosa1.setNamaDiagnosa(diagnosa.getNamaDiagnosa());
        diagnosa1.setUserId(register);
        return diagnosaRepository.save(diagnosa1);
    }

    @Override
    public Diagnosa getId(Long id) {
        var diagnosa = diagnosaRepository.findById(id).get();
        return diagnosaRepository.findById(id).orElseThrow(() -> new NotFoundException("id not found"));
    }

    @Override
    public Diagnosa editDiagnosa(Long id, Diagnosa diagnosa) {
        Diagnosa diagnosa1 = diagnosaRepository.findById(id).orElseThrow(() -> new NotFoundException("id not found"));
        diagnosa1.setNamaDiagnosa(diagnosa.getNamaDiagnosa());
        return diagnosaRepository.save(diagnosa1);
    }

    @Override
    public Map<String, Boolean> deleteDiagnosa(Long id) {
        try {
            diagnosaRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("Hapus", Boolean.TRUE);
            return res;
        } catch (Exception e) {

            Map<String, Boolean> tes = new HashMap<>();
            tes.put("Hapus", Boolean.FALSE);
            return tes;
        }
    }

    @Override
    public Page<Diagnosa> allDiangnosa(Long page, Long userId) {
        Pageable pageable = PageRequest.of(Math.toIntExact(page), 5);
        return diagnosaRepository.getAllDiangnosa(pageable, userId);
    }
}
