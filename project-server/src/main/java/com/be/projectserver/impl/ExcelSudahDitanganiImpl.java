package com.be.projectserver.impl;

import com.be.projectserver.model.ExcelSudahDitangani;
import com.be.projectserver.model.Pasien;
import com.be.projectserver.model.PeriksaPasien;
import com.be.projectserver.repository.PasienRepository;
import com.be.projectserver.repository.PeriksaPasienRepository;
import com.be.projectserver.service.ExcelSudahDitanganiService;
import com.be.projectserver.service.PeriksaPasienService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.util.List;

@Service
public class ExcelSudahDitanganiImpl implements ExcelSudahDitanganiService {

    @Autowired
    PeriksaPasienRepository periksaPasienRepository;

    @Override
    public ByteArrayInputStream findByIdPasien(Long id) {
        PeriksaPasien pasiens = periksaPasienRepository.findById(id).orElse(null);
        ByteArrayInputStream in = ExcelSudahDitangani.sudahDitanganiToExcel(pasiens);
        return in;
    }

}
