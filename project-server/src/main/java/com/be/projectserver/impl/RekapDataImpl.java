package com.be.projectserver.impl;

import com.be.projectserver.model.PeriksaPasien;
import com.be.projectserver.model.RekapData;
import com.be.projectserver.repository.PeriksaPasienRepository;
import jdk.dynalink.linker.LinkerServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.util.List;

@Service
public class RekapDataImpl {

    @Autowired
    PeriksaPasienRepository pasienRepository;

    public ByteArrayInputStream loadRekapData(String starttanggal, String endtanggal) {
        List<PeriksaPasien> periksaPasiens = pasienRepository.filterTanggal(starttanggal, endtanggal);
        ByteArrayInputStream in = RekapData.rekapDataToExcel(periksaPasiens);
        return in;
    }
}
